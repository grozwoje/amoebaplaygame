// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class AmoebaPlayGameTarget : TargetRules
{
	public AmoebaPlayGameTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "AmoebaPlayGame" } );
	}
}
