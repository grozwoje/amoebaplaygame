// Fill out your copyright notice in the Description page of Project Settings.


#include "AmoebaPlayGamePlayerController.h"

#include "AmoebaPlayGameHUD.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/PlayerInput.h"

AAmoebaPlayGamePlayerController::AAmoebaPlayGamePlayerController()
{
	//DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AAmoebaPlayGamePlayerController::SetupInputComponent()
{

	Super::SetupInputComponent();
	FInputAxisKeyMapping backKey("OpenMenu", EKeys::S, 1.f);
	GetWorld()->GetFirstPlayerController()->PlayerInput->AddAxisMapping(backKey);


	const FInputActionKeyMapping openMenu("OpenMenu", EKeys::Q, 0, 0, 0, 0);
	UPlayerInput::AddEngineDefinedActionMapping(openMenu);

	if (InputComponent)
	{
		InputComponent->BindAction("OpenMenu", IE_Pressed, this, &AAmoebaPlayGamePlayerController::OpenMenu);
	}
}

void AAmoebaPlayGamePlayerController::OpenMenu()
{
	if (AAmoebaPlayGameHUD* MenuHUD = Cast<AAmoebaPlayGameHUD>(GetHUD()))
	{
		//UGameplayStatics::SetGamePaused(GetWorld(),true);
		MenuHUD->ShowMenu();
	}

}

void AAmoebaPlayGamePlayerController::SpawnNewBall()
{
	//		Block = GetWorld()->SpawnActor<ABlockActor>(FVector(0, 0, 0),FRotator(0,0,0));
	if (!Diatom) {
		Diatom = nullptr;
		//Diatom = GetWorld()->SpawnActor<ADiatomActor>(ADiatomActor::StaticClass());
	}
	/*
	if (BallObj) {

		Diatom = GetWorld()->SpawnActor<ADiatomActor>(ADiatomActor::StaticClass());

	}*/
}