// Fill out your copyright notice in the Description page of Project Settings.


#include "AmoebaPlayGameHUD.h"
#include "UI\SAmoebaPlayGameMenuWidget.h"
#include "Widgets\SWeakWidget.h"
#include "Engine\Engine.h"
#include "Engine.h"
#include "GameFramework\HUD.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework\PlayerController.h"

void AAmoebaPlayGameHUD::BeginPlay()
{
	Super::BeginPlay();
	ShowMenu();
}

void AAmoebaPlayGameHUD::ShowMenu()
{
	UGameplayStatics::SetGamePaused(GetWorld(), true);
	if (GEngine && GEngine->GameViewport)
	{

		MenuWidget = SNew(SAmoebaPlayGameMenuWidget).OwningHUD(this);
		GEngine->GameViewport->AddViewportWidgetContent(SAssignNew(MenuWidgetContainer, SWeakWidget).PossiblyNullContent(MenuWidget.ToSharedRef()));
	}

	if (PlayerOwner)
	{
		PlayerOwner->bShowMouseCursor = true;
		PlayerOwner->SetInputMode(FInputModeUIOnly());
	}
}

void AAmoebaPlayGameHUD::RemoveMenu() 
{
	//UGameplayStatics::SetGamePaused(GetWorld(), false );
	if (GEngine && GEngine->GameViewport && MenuWidgetContainer.IsValid())
	{
		GEngine->GameViewport->RemoveViewportWidgetContent(MenuWidgetContainer.ToSharedRef());

		if (PlayerOwner)
		{

			UGameplayStatics::SetGamePaused(GetWorld(), false);

			//PlayerOwner->bShowMouseCursor = false;

			PlayerOwner->bShowMouseCursor =	true;
			PlayerOwner->SetInputMode(FInputModeGameOnly());
		}
	}
	class APlayerController * MyPC = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	MyPC->ClientSetCameraFade(true, FColor::White, FVector2D(1.0, 0.0), 1.0);

}


