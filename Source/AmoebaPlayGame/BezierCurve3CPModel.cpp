// Fill out your copyright notice in the Description page of Project Settings.


#include "BezierCurve3CPModel.h"


BezierCurve3CPModel::BezierCurve3CPModel(
	FVector(v_cp_0),
	FVector(v_cp_1),
	FVector(v_cp_2),
	int32 n,
	FVector(bp))
	:
	cp_0(FVector(v_cp_0.X, v_cp_0.Y, v_cp_0.Z)),
	cp_1(FVector(v_cp_1.X, v_cp_1.Y, v_cp_1.Z)),
	cp_2(FVector(v_cp_2.X, v_cp_2.Y, v_cp_2.Z)),
	N(n), BasePoint(FVector(bp.X, bp.Y, bp.Z))
{
	//UE_LOG(LogTemp, Log, TEXT("+++++++ Midle Vector  : (%f, %f,%f) "), bp.X, bp.Y, bp.Z);
	InitBezier(bp);
}

BezierCurve3CPModel::BezierCurve3CPModel(
	FVector(v_cp_0), FVector(v_cp_1), FVector(v_cp_2), int32 n)
	: BezierCurve3CPModel(FVector(v_cp_0), FVector(v_cp_1), FVector(v_cp_2), n,
		FVector((v_cp_0.Z - v_cp_2.Z) / 2, (v_cp_0.Y - v_cp_2.Y) / 2, .0f))
{}

BezierCurve3CPModel::BezierCurve3CPModel()
	: BezierCurve3CPModel(FVector(0.0f), FVector(0.5f), FVector(1.0f), 64,
		FVector(0.0f, 0.0f, 0.0f))
{}

BezierCurve3CPModel::~BezierCurve3CPModel()
{}


int32 BezierCurve3CPModel::Get_N()
{
	return N;
}

void BezierCurve3CPModel::Set_N(int32 n)
{
	N = n;
}

FVector BezierCurve3CPModel::GetCasteljauHornerPoint(float t)
{
	const int nn{ 3 };
	FVector wsp[nn];
	wsp[2] = cp_0;
	wsp[1] = -2 * cp_0 + 3 * cp_1;
	wsp[0] = cp_0 - 2 * cp_1 + cp_2;

	FVector w = wsp[0];
	for (int i = 1; i < nn; i++)
		w = w*t + wsp[i];

	return w;
}

FVector BezierCurve3CPModel::Get_CP(int8 cp)
{
	if (cp % 3 == 0) {
		return cp_0;
	}
	else if (cp % 3 == 1) {
		return cp_1;
	}
	else return cp_2;
}

// xxx !!! 
void BezierCurve3CPModel::Set_CP(int8 cp, FVector vector)
{

	N_Points.Empty();

	if (cp/*% 3*/ == 0) {
		cp_0 = vector;
	}
	if (cp /*% 3*/ == 1) {
		cp_1 = vector;
	}
	if (cp == 2) {
		cp_2 = vector;
	}
	InitBezier(GetBasePoint());
}

void BezierCurve3CPModel::Set_CP(FVector coord_cp_0, FVector coord_cp_1, FVector coord_cp_2)
{
	N_Points.Empty();
	cp_0 = coord_cp_0;
	cp_1 = coord_cp_1;
	cp_2 = coord_cp_2;
	InitBezier(GetBasePoint());
}

void BezierCurve3CPModel::InitBezier(FVector bp)
{
	N_Points.Init(FVector(.0f, 0.0f, .0f), N + 1);

	float dn = 1 / static_cast <float>(N);

	for (int32 i = 0; i <= N; i++)
	{
		N_Points[i].X = cp_0.X * (1 - i* dn) *  (1 - i* dn)
			+ 2 * cp_1.X * i * dn * (1 - i*dn) + cp_2.X * i*dn * i*dn;
		N_Points[i].Y = cp_0.Y * (1 - i* dn) *  (1 - i* dn)
			+ 2 * cp_1.Y * i * dn * (1 - i*dn) + cp_2.Y * i*dn * i*dn;
		N_Points[i].Z = cp_0.Z * (1 - i* dn) *  (1 - i* dn)
			+ 2 * cp_1.Z * i * dn * (1 - i*dn) + cp_2.Z * i*dn * i*dn;
		/*UE_LOG(LogTemp, Warning, TEXT("InitBezier[%i] : (%f, %f, %f)"),
		i, N_Points[i].X,N_Points[i].Y,N_Points[i].Z);*/

	}
	N_Points.Add(bp);
}

FVector* BezierCurve3CPModel::GetCoordinatePointer(int point_n)
{
	return &N_Points[point_n];
}


FVector BezierCurve3CPModel::GetCoordinate(int coord)
{
	return N_Points[coord];
}

FVector BezierCurve3CPModel::GetBasePoint()
{
	return BasePoint;
}
