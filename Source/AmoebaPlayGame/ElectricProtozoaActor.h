// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ElectricProtozoaActor.generated.h"

UCLASS()
class AMOEBAPLAYGAME_API AElectricProtozoaActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AElectricProtozoaActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;    
private:

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UProceduralMeshComponent* DownMesh;


	TArray<FVector> DownVertices;
	TArray<int32> DownTriangles;
	void AddDownTriangle(int32 V1, int32 V2, int32 V3);
	void GenerateDownMesh();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UProceduralMeshComponent* UpMesh;

	TArray<FVector> UpVertices;
	TArray<int32> UpTriangles;
	void AddUpTriangle(int32 V1, int32 V2, int32 V3);
	void GenerateUpMesh();


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UProceduralMeshComponent* LeftMesh;

	TArray<FVector> LeftVertices;
	TArray<int32> LeftTriangles;
	void AddLeftTriangle(int32 V1, int32 V2, int32 V3);
	void GenerateLeftMesh();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UProceduralMeshComponent* RightMesh;

	TArray<FVector> RightVertices;
	TArray<int32> RightTriangles;
	void AddRightTriangle(int32 V1, int32 V2, int32 V3);
	void GenerateRightMesh();


	UPROPERTY()
	class UMaterial* BaseMaterial;

	UPROPERTY()
	class UMaterialInstance* PinkMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* CubeMesh;

};
