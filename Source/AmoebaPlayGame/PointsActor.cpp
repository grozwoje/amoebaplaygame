// Fill out your copyright notice in the Description page of Project Settings.
#include "PointsActor.h"
#include "Components/TextRenderComponent.h"


// Sets default values
APointsActor::APointsActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PointText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("CountNumber"));
	PointText->SetHorizontalAlignment(EHTA_Center);
	PointText->SetWorldSize(50.0f);
	RootComponent = PointText;


}

// Called when the game starts or when spawned
void APointsActor::BeginPlay()
{
	Super::BeginPlay();
	SetPoints(55);
	AddPoint();
	
}

// Called every frame
void APointsActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}


int32 APointsActor::GetPoints()
{
	return Points;
}
void APointsActor::SetPoints(int32 points)
{
	Points = points;
}

void APointsActor::AddPoint()
{

	Points++;
	PointText->SetText(FString::FromInt(Points));
}
