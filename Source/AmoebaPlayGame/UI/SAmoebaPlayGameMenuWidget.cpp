// Fill out your copyright notice in the Description page of Project Settings.


#include "SAmoebaPlayGameMenuWidget.h"
#include "GameFramework\PlayerController.h"
#include "AmoebaPlayGameHUD.h"
#include "Styling\CoreStyle.h"
#include "Widgets/Input/SSlider.h"

#define LOCTEXT_NAMESPACE "MainMenu"

void SAmoebaPlayGameMenuWidget::Construct(const FArguments & InArgs)
{
	FLinearColor ColorGreen = FLinearColor(.032f, 0.229f, .05f,1.f);
	FLinearColor ColorBlue = FLinearColor(.132f,.509f,.796f, 1.f);

	bCanSupportFocus = true;

	OwningHUD = InArgs._OwningHUD;

	const FMargin ContentPading = FMargin(600.f, 380.f);
	const FMargin ButtonPadding = FMargin(10.f);


	const FText TitleText = LOCTEXT("GameTitle", "Amoeba Play Paddle");
	const FText PlayText = LOCTEXT("PlayGame", "Play");
	const FText SettingsText = LOCTEXT("Settings", "Settings");
	const FText QuitText = LOCTEXT("QuitGame", "Quit Game");


	FSlateFontInfo ButtonTextStyle = FCoreStyle::Get().GetFontStyle("EmbossedText");
	ButtonTextStyle.Size = 40.f;

	FSlateFontInfo TitleTextStyle = ButtonTextStyle;
	ButtonTextStyle.Size = 40.f;
	//FString ImagePath = FPaths::GameContentDir() / TEXT("Sprites/logo.png");
	//Texture2D'/Game/StarterContent/Sprites/logo.logo'
	//PlaneMesh(TEXT("/Game/StarterContent/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
	//FName BrushName = FName(*ImagePath);
	ChildSlot
		[
			SNew(SOverlay)
			
			+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SImage)
			//.ColorAndOpacity(FColor::Black)
			//.Image(new FSlateImageBrush(BrushName, FVector2D(128, 128)))
			//.ColorAndOpacity(ColorBlue)

		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(ContentPading)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		[
			SNew(STextBlock)
			.Font(TitleTextStyle)
			.Text(TitleText)
			.ColorAndOpacity(ColorGreen)
			.Justification(ETextJustify::Center)
		]
		+ SVerticalBox::Slot()
		.Padding(ButtonPadding)
		[
			SNew(SButton)
			.OnClicked(this, &SAmoebaPlayGameMenuWidget::OnPlayClicked)
			//.ButtonColorAndOpacity(FColor::Green)
			.ButtonColorAndOpacity(ColorBlue)
			[
				SNew(STextBlock)
				.Font(ButtonTextStyle)
				.Text(PlayText)
				.ColorAndOpacity(ColorGreen)
				.Justification(ETextJustify::Center)
			]
		]
		
		+ SVerticalBox::Slot()
		[
			SNew(SProgressBar)
			.Percent_Lambda([&]()
		{
			return GetCompletePercent();
		})
			
		]
		]
	
		/*
			+SVerticalBox::Slot()
			.Padding(ButtonPadding)
			[
				SNew(SSlider)
				.SliderBarColor(ColorGreen)
				.MinValue(10)
				.MaxValue(100)
				.StepSize(10)
			//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=====================================")));

				//.ColorAndOpacity(ColorGreen)
				//.Justification(ETextJustify::Center)
			]
			*
		+ SVerticalBox::Slot()
		.Padding(ButtonPadding)
		[
			SNew(SButton)
				.OnClicked(this, &SAmoebaPlayGameMenuWidget::OnQuitClicked)
				//.ButtonColorAndOpacity(FColor::Green)
				.ButtonColorAndOpacity(ColorBlue)
				[
					SNew(STextBlock)
					.Font(ButtonTextStyle)
					.Text(QuitText)
					.ColorAndOpacity(ColorGreen)
					.Justification(ETextJustify::Center)
				]
			]

		]
		/*I
		+ SOverlay::Slot()
		.Padding(ButtonPadding)
		[
			SNew(SNumericEntryBox)
			.Font(ButtonTextStyle)
			.ColorAndOpacity(ColorGreen)
			.Justification(ETextJustify::Center)
		]
		*/

	];
}
FReply SAmoebaPlayGameMenuWidget::OnPlayClicked() const
{
	if (OwningHUD.IsValid())
	{
		OwningHUD->RemoveMenu();
	}

	return FReply::Handled();
}

FReply SAmoebaPlayGameMenuWidget::OnQuitClicked() const
{
	if (OwningHUD.IsValid())
	{
		if (APlayerController* PC = OwningHUD->PlayerOwner)
		{
			PC->ConsoleCommand("quit");
		}
	}

	return FReply::Handled();
}


float SAmoebaPlayGameMenuWidget::GetCompletePercent()
{
	//return completePercent;
	return .5f;
}
#undef LOCTEXT_NAMESPACE