// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "SlateBasics.h"
#include "SlateExtras.h"



class SAmoebaPlayGameMenuWidget : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SAmoebaPlayGameMenuWidget) {}

	SLATE_ARGUMENT(TWeakObjectPtr<class AAmoebaPlayGameHUD>, OwningHUD)

		SLATE_END_ARGS()

		void Construct(const FArguments& InArgs);

	FReply OnPlayClicked() const;
	FReply OnQuitClicked() const;

	TWeakObjectPtr<class AAmoebaPlayGameHUD> OwningHUD;

	virtual bool SupportsKeyboardFocus() const override { return true; }
	float GetCompletePercent();
};
