// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "BrickActor.generated.h"


USTRUCT(immutable, noexport, BlueprintType)
struct FBounceDirection
{
	//GENERATED_USTRUCT_BODY()
	UPROPERTY()	bool NewSide;

	UPROPERTY()	UBoxComponent* ChoseCollision;

	FBounceDirection();

	FBounceDirection(bool NewSide, UBoxComponent* NewChoseCollision)
	{
		NewSide = NewSide;
		ChoseCollision = NewChoseCollision;
	}

};

UCLASS()
class AMOEBAPLAYGAME_API ABrickActor : public AActor
{
	GENERATED_BODY()



	
public:	
	// Sets default values for this actor's properties
	ABrickActor();


	/** Dummy root component */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	/** StaticMesh component for the clickable block */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* BlockMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns BlockMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }

	UPROPERTY()	UBoxComponent* LeftCollision;
	UPROPERTY()	UBoxComponent* RightCollision;
	UPROPERTY()	UBoxComponent* TopCollision;
	UPROPERTY()	UBoxComponent* BottomCollision;

};
