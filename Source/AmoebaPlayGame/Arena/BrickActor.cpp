// Fill out your copyright notice in the Description page of Project Settings.


#include "BrickActor.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"

// Sets default values
ABrickActor::ABrickActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeMesh"));

	BlockMesh->SetStaticMesh(ShapeMeshAsset.Object);
	BlockMesh->SetupAttachment(RootComponent);
	BlockMesh->SetCollisionProfileName("brick");

}

// Called when the game starts or when spawned
void ABrickActor::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ABrickActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

