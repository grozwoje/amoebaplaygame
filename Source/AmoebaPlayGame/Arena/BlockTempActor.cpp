// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockTempActor.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/Engine.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/Actor.h"
#include "AmoebaPawn.h"
#include "BlockActor.h"
#include "BrickActor.h"
#include "DiatomActor.h"
#include "CeilingActor.h"
#include "Materials/MaterialInstance.h"
#include "DrawDebugHelpers.h"
// Sets default values
ABlockTempActor::ABlockTempActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> LightGreenMaterial;

		FConstructorStatics()
			: BaseMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, LightGreenMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/LightGreenMaterial.LightGreenMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;


	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));

	BlockShape = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeMesh"));
	BlockShape->SetStaticMesh(ShapeMeshAsset.Object);
	BlockShape->SetupAttachment(RootComponent);


	BlockMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MoveShape"));
	BlockMovement->UpdatedComponent = BlockShape;

	BlockMovement->InitialSpeed = 80.f;
	BlockMovement->MaxSpeed = 3000.f;
	BlockMovement->bRotationFollowsVelocity = false;
	BlockMovement->bShouldBounce = false;
	BlockMovement->ProjectileGravityScale = 0.f;
	BlockMovement->bConstrainToPlane = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));

	BoxComponent->SetupAttachment(BlockShape);

	BoxComponent->SetRelativeLocation(FVector(.0f, .0f, 50.0f));

	LightGreenMaterial = ConstructorStatics.LightGreenMaterial.Get();
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();

	BlockShape->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	BlockShape->SetMaterial(0, ConstructorStatics.LightGreenMaterial.Get());

	BlockMovement->Velocity = FVector(0.0f, 0.0f, -0.020f);
}

// Called when the game starts or when spawned
void ABlockTempActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABlockTempActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (BlockMovement->Velocity == FVector(0, 0, 0)) {
		this->Destroy();
	}
	if (GetActorLocation().Z < -130)
	{
		Destroy();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Destroy Block"));
	}

}


void ABlockTempActor::OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity)
{

}

void ABlockTempActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PROJECTILE HIT - ON_HIT"));
	if (Hit.Actor->IsA<ADiatomActor>())
	{
		Destroy();
	}
}

void ABlockTempActor::OnStop(const FHitResult& Hit)
{
}