// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlockActor.generated.h"

UCLASS()
class AMOEBAPLAYGAME_API ABlockActor : public AActor
{
	GENERATED_BODY()



		/** Dummy root component */
		UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;


	//bool bUpDownHit;
	//bool bSideHit;

public:
	// Sets default values for this actor's properties
	ABlockActor();
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	/** StaticMesh component for the clickable block */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* BlockMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Pointer to white material used on the focused block */
	UPROPERTY()
		class UMaterial* BaseMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
		class UMaterialInstance* BlueMaterial;


public:

	virtual void Tick(float DeltaTime) override;

	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }

	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }

	void RandomReplaceActorLocation();
	
	UPROPERTY()
	class UBoxComponent* UpBox;

	UPROPERTY()
	class UBoxComponent* DownBox;

	UPROPERTY()
	class UBoxComponent* RightBox;

	UPROPERTY()
	class UBoxComponent* LeftBox;

	UFUNCTION()
	void OnOverlapBeginUpDown(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEndUpDown(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void OnOverlapBeginSide(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEndSide(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector FitLocation, FVector HitNormal, FVector normalimpulse, const FHitResult& Hit) override;
	//bool GetUpDownHit();

	//bool GetSideHit();
	
};
