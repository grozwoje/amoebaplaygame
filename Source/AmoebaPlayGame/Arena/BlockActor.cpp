// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockActor.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Engine/CollisionProfile.h"
#include "Components/BoxComponent.h"

// Sets default values
ABlockActor::ABlockActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeMeshBlock"));
	BlockMesh->SetStaticMesh(ShapeMeshAsset.Object);
	BlockMesh->SetupAttachment(RootComponent);
	BlockMesh->BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics, true);

	//BlockMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	//--
	//BlockMesh->BodyInstance.SetResponseToAllChannels(ECR_Block);
//BlockMesh->SetNotifyRigidBodyCollision(true);


	BlockMesh->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
	//BlockMesh->BodyInstance.SetCollisionProfileName(TEXT("WorldStatic"));
	//BlockMesh->SetCollisionProfileName(TEXT("Pawn"));

	BlockMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	//BlockMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Block);
	//BlockMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Block);
	//BlockMesh->SetColl
	BlockMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	//BlockMesh->CollisionObjectType("block");
	BlockMesh->SetRelativeScale3D(FVector(.35f));


}

// Called when the game starts or when spawned
void ABlockActor::BeginPlay()
{
	Super::BeginPlay();
	///UpBox->OnComponentBeginOverlap.AddDynamic(this, &ABlockActor::OnOverlapBeginUpDown);
	//UpBox->OnComponentEndOverlap.AddDynamic(this, &ABlockActor::OnOverlapEndUpDown);

	//DownBox->OnComponentBeginOverlap.AddDynamic(this, &ABlockActor::OnOverlapBeginUpDown);
	//DownBox->OnComponentEndOverlap.AddDynamic(this, &ABlockActor::OnOverlapEndUpDown);

	//LeftBox->OnComponentBeginOverlap.AddDynamic(this, &ABlockActor::OnOverlapBeginSide);
	//LeftBox->OnComponentEndOverlap.AddDynamic(this, &ABlockActor::OnOverlapEndSide);

	//RightBox->OnComponentBeginOverlap.AddDynamic(this, &ABlockActor::OnOverlapBeginSide);
	//RightBox->OnComponentEndOverlap.AddDynamic(this, &ABlockActor::OnOverlapEndSide);
	BlockMesh->OnComponentHit.AddDynamic(this, &ABlockActor::OnHit);

	//BlockMesh->GetColl
	//BlockMesh->GetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("GetCollisionProfileName: %s"), *BlockMesh->GetCollisionProfileName().ToString()));
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("GetCollisionResponseToChannel: %s"), *BlockMesh->GetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1).ToString()));
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("GetCollisionObjectType: %s"), *BlockMesh->GetCollisionObjectType( ).ToString()));

	

}

// Called every frame
void ABlockActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorLocation(GetActorLocation() + FVector(0.f, 0.f, -0.5f));
	if (GetActorLocation().Z < -130)
	{
		Destroy();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Destroy Block"));
	}
}

void ABlockActor::RandomReplaceActorLocation()
{
	float X = FMath::RandRange(-90, 90);
	float Z = FMath::RandRange(-40, 60);
	SetActorLocation(FVector(X, 0.f, Z));
}

void ABlockActor::OnOverlapBeginUpDown(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		//bUpDownHit = true;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Overlap Begin UpDown"));
		//Destroy();
		//Destroy();
	}
}

void ABlockActor::OnOverlapEndUpDown(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		//bUpDownHit = false;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Overlap End UpDown"));
		//Destroy();
		//Destroy();
	}
}


void ABlockActor::OnOverlapBeginSide(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		//bSideHit = true;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Overlap Begin Side"));
		//Destroy();
	}
}

void ABlockActor::OnOverlapEndSide(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		//bSideHit = false;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Overlap End Side"));
		Destroy();
	}
}

/*
bool ABlockActor::GetUpDownHit()
{
	return bUpDownHit;
}

bool ABlockActor::GetSideHit()
{
	return bSideHit;
}
*/

void ABlockActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("PROJECTILE HIT - ON_HIT"));
}

void ABlockActor::NotifyHit(UPrimitiveComponent* MyComp, AActor* other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector FitLocation, FVector HitNormal, FVector normalimpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("NotifyHit"));
}