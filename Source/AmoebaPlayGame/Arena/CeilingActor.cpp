// Fill out your copyright notice in the Description page of Project Settings.


#include "CeilingActor.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"

// Sets default values
ACeilingActor::ACeilingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeMesh"));
	BlockMesh->SetStaticMesh(ShapeMeshAsset.Object);
	BlockMesh->SetupAttachment(RootComponent);
	BlockMesh->BodyInstance.SetCollisionProfileName("brick");


}

// Called when the game starts or when spawned
void ACeilingActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACeilingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

