// Fill out your copyright notice in the Description page of Project Settings.


#include "BackgroundActor.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
ABackgroundActor::ABackgroundActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
		// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		//ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/StarterContent/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			//, OrangeMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(15.f, 0.10f, 34.f));
	BlockMesh->SetRelativeLocation(FVector(0.f, -100.f, 0.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);

	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	//OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();

	BlockMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());




	//BlockMesh->SetMaterial(0, OrangeMaterial);
	
}

// Called when the game starts or when spawned
void ABackgroundActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABackgroundActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

