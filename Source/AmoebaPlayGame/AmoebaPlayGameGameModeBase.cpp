// Fill out your copyright notice in the Description page of Project Settings.


#include "AmoebaPlayGameGameModeBase.h"
#include "AmoebaPlayGamePlayerController.h"
#include "AmoebaPawn.h"
#include "AmoebaPlayGameCameraActor.h"
#include "Engine/World.h"
#include "AmoebaPlayGameHUD.h"
#include "DiatomActor.h"
#include "Arena\BlockActor.h"
#include "Arena\BrickActor.h"
#include "Arena\CeilingActor.h"
#include "BackgroundActor.h"
#include "ElectricProtozoaActor.h"

AAmoebaPlayGameGameModeBase::AAmoebaPlayGameGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;


	DefaultPawnClass = AAmoebaPawn::StaticClass();
	PlayerControllerClass = AAmoebaPlayGamePlayerController::StaticClass();
	HUDClass = AAmoebaPlayGameHUD::StaticClass();

	CountdownTime = 5;
}


void AAmoebaPlayGameGameModeBase::StartPlay()
{
	Super::StartPlay();
	class APlayerController * MyPC = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	MyPC->ClientSetCameraFade(true, FColor::White, FVector2D(1.0, 0.0), 1.0);
	
	UWorld* const World = GetWorld();
	if (World)
	{
		AAmoebaPlayGameCameraActor* Camera = GetWorld()->SpawnActor<AAmoebaPlayGameCameraActor>(AAmoebaPlayGameCameraActor::StaticClass());

		World->GetFirstPlayerController()->SetViewTarget(Camera);
		
		//ADiatomActor*
		Diatom = GetWorld()->SpawnActor<ADiatomActor>(ADiatomActor::StaticClass());
		//''>
		/*
		ADiatomActor* Diatom_1 = GetWorld()->SpawnActor<ADiatomActor>(ADiatomActor::StaticClass());
		Diatom_1->SetActorLocation(FVector(-80.0f, .0f, 80.0f));
		

		ADiatomActor* Diatom_2 = GetWorld()->SpawnActor<ADiatomActor>(ADiatomActor::StaticClass());
		Diatom_2->SetActorLocation(FVector(80.0f, .0f, 70.0f));
		*/
		//ABlockActor* 
		//Block = GetWorld()->SpawnActor<ABlockActor>(ABlockActor::StaticClass());
		//Block->SetActorScale3D(FVector(1.f, 1.f, .2f));
		
		ABrickActor* LeftBrick = GetWorld()->SpawnActor<ABrickActor>(ABrickActor::StaticClass());
		LeftBrick->SetActorLocation(FVector(-180.0f,.0f, -130.0f));
		LeftBrick->SetActorScale3D(FVector(.2f, 1.f, 2.5f));

		ABrickActor* RightBrick = GetWorld()->SpawnActor<ABrickActor>(ABrickActor::StaticClass());
		RightBrick->SetActorLocation(FVector(180.0f, .0f, -130.0f));
		RightBrick->SetActorScale3D(FVector(.2f, 1.f, 2.5f));

		ACeilingActor* Ceiling = GetWorld()->SpawnActor<ACeilingActor>(ACeilingActor::StaticClass());
		Ceiling->SetActorLocation(FVector(0.0f, .0f, 100.0f));
		Ceiling->SetActorScale3D(FVector(3.5f, 1.f, .2f));


		ABackgroundActor* Background = GetWorld()->SpawnActor<ABackgroundActor>(ABackgroundActor::StaticClass());
		Background->SetActorLocationAndRotation(FVector(0.0f, .0f, .0f), FRotator(0, 0, 0));

		//AElectricProtozoaActor* ElProtozoa = GetWorld()->SpawnActor<AElectricProtozoaActor>(AElectricProtozoaActor::StaticClass());
		//ElProtozoa->SetActorLocation(FVector(0, 0, 0));
	}
	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AAmoebaPlayGameGameModeBase::AdvanceTimer, 1.0f, true);
}

void AAmoebaPlayGameGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AAmoebaPlayGameGameModeBase::AdvanceTimer, 1.0f, true);
	if (CountdownTime == 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=====================================")));

	}
}

//GetWorldTimerManager().SetTimer(CountdownTimerHandle, this,&ASimpleGeometryPawn::AdvanceTimer, 1.0f, true);


void AAmoebaPlayGameGameModeBase::AdvanceTimer()
{
	--CountdownTime;
	if (CountdownTime < 1)
	{
		GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
		//CountdownTime = 5;
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=====================================")));
	}
}
