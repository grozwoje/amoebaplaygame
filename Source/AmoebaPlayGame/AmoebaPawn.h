// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Engine.h"
#include "BezierCurve3CPModel.h"
#include "GameFramework/Pawn.h"
#include "MoveTringleActor.h"
#include "ProceduralMeshComponent.h"
#include "Arena\BlockActor.h"
#include "DiatomActor.h"
#include "Arena\BlockTempActor.h"
#include "AmoebaPawn.generated.h"

UCLASS()
class AMOEBAPLAYGAME_API AAmoebaPawn : public APawn
{
	GENERATED_BODY()

public:
	AAmoebaPawn();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UPROPERTY()
	class UMaterial* BaseMaterial;


	UPROPERTY()
	class UMaterialInstance* PinkMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* KernelMesh;
public:

	void MovePaddleX(float Scale);

	void GenerateMinusAomebaLeaf_2_Mesh();
	void GenerateMinusAomebaLeaf_1_Mesh();
	void GenerateMainAomebaLeafMesh();
	void GeneratePlusAomebaLeaf_1_Mesh();
	void GeneratePlusAomebaLeaf_2_Mesh();

	void GenerateMinusTriangle();
	void GeneratePlusTriangle();

	UFUNCTION()		void AomebaMove();

	FVector CurrentVelocity;

	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* CustomMesh;

	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* MinusAomebaLeaf_2_Mesh;
	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* MinusAomebaLeaf_1_Mesh;
	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* MainAomebaLeafMesh;
	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* PlusAomebaLeaf_1_Mesh;
	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* PlusAomebaLeaf_2_Mesh;

	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* MinusTriangleMesh;
	UPROPERTY(VisibleAnywhere)	UProceduralMeshComponent* PlusTriangleMesh;


	UPROPERTY(VisibleAnywhere, Category = ProcMesh)		UMaterial* Material;

	BezierCurve3CPModel* BezierBranch;

	BezierCurve3CPModel* MinusAomebaLeaf_2;
	BezierCurve3CPModel* MinusAomebaLeaf_1;
	BezierCurve3CPModel* MainAomebaLeaf;
	BezierCurve3CPModel* PlusAomebaLeaf_1;
	BezierCurve3CPModel* PlusAomebaLeaf_2;

	UPROPERTY()		TArray<FVector> VertMinusAomebaLeaf_2;
	UPROPERTY()		TArray<int32> TriMinusAomebaLeaf_2;
	UPROPERTY()		TArray<FLinearColor> VertColorsMinusAomebaLeaf_2;

	UPROPERTY()		TArray<FVector> VertMinusAomebaLeaf_1;
	UPROPERTY()		TArray<int32> TriMinusAomebaLeaf_1;
	UPROPERTY()		TArray<FLinearColor> VertColorsMinusAomebaLeaf_1;

	UPROPERTY()		TArray<FVector> VertMainAomebaLeaf;
	UPROPERTY()		TArray<int32> TriMainAomebaLeaf;
	UPROPERTY()		TArray<FLinearColor> VertColorsMainAomebaLeaf;

	UPROPERTY()		TArray<FVector> VertPlusAomebaLeaf_1;
	UPROPERTY()		TArray<int32> TriPlusAomebaLeaf_1;
	UPROPERTY()		TArray<FLinearColor> VertColorsPlusAomebaLeaf_1;

	UPROPERTY()		TArray<FVector> VertPlusAomebaLeaf_2;
	UPROPERTY()		TArray<int32> TriPlusAomebaLeaf_2;
	UPROPERTY()		TArray<FLinearColor> VertColorsPlusAomebaLeaf_2;

	UPROPERTY()		TArray<FVector> VertMinusTriangle;
	UPROPERTY()		TArray<int32> TriMinusTriangle;
	UPROPERTY()		TArray<FLinearColor> VertColorsMinusTriangle;

	UPROPERTY()		TArray<FVector> VertPlusTriangle;
	UPROPERTY()		TArray<int32> TriPlusTriangle;
	UPROPERTY()		TArray<FLinearColor> VertColorsPlusTriangle;

	void AddTriVertMinusAomebaLeaf_2(int32 V1, int32 V2, int32 V3);
	void AddTriVertMinusAomebaLeaf_1(int32 V1, int32 V2, int32 V3);
	void AddTriVertMainAomebaLeaf(int32 V1, int32 V2, int32 V3);
	void AddTriVertPlusAomebaLeaf_1(int32 V1, int32 V2, int32 V3);
	void AddTriVertPlusAomebaLeaf_2(int32 V1, int32 V2, int32 V3);

	void AddMinusTriangle(int32 V1, int32 V2, int32 V3);
	void AddMPlusTriangle(int32 V1, int32 V2, int32 V3);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:

	UPROPERTY()		int32 N;
	UPROPERTY()		FVector BasePoint_0;

	UPROPERTY()		FVector CP_Minus_2_First;
	UPROPERTY()		FVector	CP_Minus_2_Last;

	UPROPERTY()		FVector CP_Minus_2_Temp;
	UPROPERTY()		FVector CP_Minus_1_Temp;

	UPROPERTY()		FVector	CP_Main_First;
	UPROPERTY()		FVector	CP_Main_Core;
	UPROPERTY()		FVector	CP_Main_Last;

	UPROPERTY()		FVector CP_Plus_2_First;
	UPROPERTY()		FVector	CP_Plus_2_Last;

	UPROPERTY()		FVector CP_Plus_2_Temp;
	UPROPERTY()		FVector CP_Plus_1_Temp;


	UPROPERTY()		FVector BaseMinus;
	//UPROPERTY()		FVector BaseMinus_1;

	UPROPERTY()		FVector BasePlus;

	UPROPERTY()		FVector Temp_Main_Core;

	FHitResult Hit;

	class TArray<ABlockActor*> Blocks;
	ABlockActor* Block;

	class TArray<ABlockTempActor*> TempBlocks;
	ABlockTempActor* TempBlock;

	class TArray<ABlockTempActor*> TempBlocks_1;
	ABlockTempActor* TempBlock_1;

	class TArray<ABlockTempActor*> TempBlocks_2;
	ABlockTempActor* TempBlock_2;

	class TArray<ABlockTempActor*> TempBlocks_3;
	ABlockTempActor* TempBlock_3;
public:

	UPROPERTY()	class AMoveTringleActor* LeftTriangle;

	UPROPERTY()	class AMoveTringleActor* RightTriangle;

	UPROPERTY(EditAnywhere)
	float CountdownTime;
	void AdvanceTimer();
	FTimerHandle CountdownTimerHandle;

	UPROPERTY(EditAnywhere)
	float CountdownTime_1;
	void AdvanceTimer_1();
	FTimerHandle CountdownTimerHandle_1;

	UPROPERTY(EditAnywhere)
	float CountdownTime_2;
	void AdvanceTimer_2();
	FTimerHandle CountdownTimerHandle_2;


	//...
private:
	class APointsActor* HeroPoints;
	class APointsActor* EnemyPoints;
};
