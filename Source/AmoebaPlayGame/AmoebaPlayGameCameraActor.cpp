// Fill out your copyright notice in the Description page of Project Settings.


#include "AmoebaPlayGameCameraActor.h"

#include "Camera/CameraComponent.h"

AAmoebaPlayGameCameraActor::AAmoebaPlayGameCameraActor()
	: Super()
{
	UCameraComponent* Camera = GetCameraComponent();

	//Set the Camera To Orthographic.  Also set the Location and Rotation on the Camera: X is left and right, Z is up and down, Y is depth relative to this camera
	Camera->ProjectionMode = ECameraProjectionMode::Orthographic;
	Camera->SetRelativeLocation(FVector(0.0f, 100.0f, 0.0f));
	Camera->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));
	Camera->SetAbsolute(true, true, true);

}

//Get the Dimensions of what our camera can view
FVector2D AAmoebaPlayGameCameraActor::GetViewDimensions()
{
	UCameraComponent* Camera = GetCameraComponent();
	FVector2D dimensions;

	//The width is the Orthographic view Width.  Calculate the height from the Aspect ratio and OrthoWidth
	dimensions.X = Camera->OrthoWidth;
	dimensions.Y = 1 / (Camera->AspectRatio / Camera->OrthoWidth);

	return dimensions;
}
