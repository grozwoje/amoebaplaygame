// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "AmoebaPawnGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class AMOEBAPLAYGAME_API UAmoebaPawnGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
