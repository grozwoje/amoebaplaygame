// Fill out your copyright notice in the Description page of Project Settings.
/*

Ax + By + C = 0
A = -a
B = 1
C = -b

f(x) = y = a*x + b

a = (y_1 - y_2)/(x_1 - x_2)
b = y_1 - a*x_1 = y_1 - (y_1-y_2)/(x_1-x_2)*x_1

if(x_1 != x_2) {
	A = -(y_1 - y_2)/(x_1 - x_2);
    B = 1
	C = (y_1 - y_2)/(x_1 - x_2) * x_1 - y_1
} else {
	C = -x_1
	B = 0 
	A = 1
}
-----------------------------------------------
point of intersection of lines 

A_1*x + B_1*y + C_2 = 0
A_1*x + B_2*y + C_2 = 0

x = (C_2/B_2 - C_1/B_1) / (A_1/B_1 - A_2/_B_2)

y = (C_2/A_2 - B_1 / A_1) / (B_1/A_1 - B_2/A_2)



perpendicular line factor
*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "AmoebaPawn.h"
#include "AmoebaPlayGamePlayerController.h"
#include "DiatomActor.generated.h"

UCLASS()
class AMOEBAPLAYGAME_API ADiatomActor : public AActor
{
	GENERATED_BODY()


public:
	ADiatomActor();

protected:
	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* DiatomShape;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UProjectileMovementComponent* DiatomMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USphereComponent* SphereComponent;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity);

	UFUNCTION()
	void OnStop(const FHitResult& Hit);

	UPROPERTY()
		class UMaterial* BaseMaterial;

	//UPROPERTY()
	//class UMaterialInstance* BlueMaterial;

	UPROPERTY()
		class UMaterialInstance* LightGreenMaterial;

private:

	FVector Trace;
	class AAmoebaPawn* Amoeba;

	FVector2D P_T;

	FVector GeneralLineEquation(FVector2D& First, FVector2D& Second);


	FVector2D IntersectionLines(FVector& Straight_0, FVector& Straight_1);

	float PerpendicularLineFactor(float& A);

	float ShiftFactor(FVector& Line, FVector2D& Point);

	float DirectionalCoordinate(FVector2D& P_1, FVector2D& P_2);
	float ShiftCoodinate(FVector2D& P_1, FVector2D& P_2);
	float TheoremOfCosines(FVector2D P_A, FVector2D P_B, FVector2D P_C );
	float TheoremOfCosines(float b, float c, float angle);

	FVector2D PointOnPerpendicularLine();
public:
	class AAmoebaPlayGamePlayerController* AmoebaPlayerController;

protected:
	UPROPERTY(EditAnywhere)
	int32 CountdownTime;
	void AdvanceTimer();
	FTimerHandle CountdownTimerHandle;

private:
	void VaightAction();
};
