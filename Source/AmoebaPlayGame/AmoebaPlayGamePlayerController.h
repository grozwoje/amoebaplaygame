// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "DiatomActor.h"
#include "AmoebaPlayGamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class AMOEBAPLAYGAME_API AAmoebaPlayGamePlayerController : public APlayerController
{
	GENERATED_BODY()


	AAmoebaPlayGamePlayerController();

	virtual void SetupInputComponent() override;

	void OpenMenu();
public:
	void SpawnNewBall();
	class ADiatomActor* Diatom;

	//UPROPERTY(EditAnywhere)
		//TSubclassOf<ADiatomActor> BallObj;
};
