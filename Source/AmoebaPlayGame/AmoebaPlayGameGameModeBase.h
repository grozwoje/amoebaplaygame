// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AmoebaPlayGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class AMOEBAPLAYGAME_API AAmoebaPlayGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	AAmoebaPlayGameGameModeBase();

	virtual void StartPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

private:
	class ABlockActor* Block;
	class ADiatomActor* Diatom;
public:
		UPROPERTY(EditAnywhere)
		int32 CountdownTime;

		void AdvanceTimer();
		//void CountdownHasFinished();
		FTimerHandle CountdownTimerHandle;
};
