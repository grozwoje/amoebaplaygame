// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "MoveTringleActor.generated.h"

UCLASS()
class AMOEBAPLAYGAME_API AMoveTringleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AMoveTringleActor();

protected:

	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
	class UMaterial* BaseMaterial;

	//UPROPERTY()
	//class UMaterialInstance* BlueMaterial;

	UPROPERTY()
	class UMaterialInstance* OrangeMaterial;

	UFUNCTION()
	void BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);

	UFUNCTION()
	void OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent);


	UPROPERTY(VisibleAnywhere, Category = "Switch Components")
	class UPointLightComponent* PointLight1;


	UPROPERTY(VisibleAnywhere, Category = "Switch Variables")
	float DesiredIntensity;

	UFUNCTION()
	void ToggleLight();


private:
	void AddTriangle(int32 V1, int32 V2, int32 V3);

public:

	void GenerateTriangleMesh();

private:
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* CustomMesh;


	TArray<FVector> Vertices;

	TArray<int32> Triangles;

	TArray<FLinearColor> VertexColors;

};
