// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "AmoebaPlayGameCameraActor.generated.h"

/**
*
*/
UCLASS()
class AMOEBAPLAYGAME_API AAmoebaPlayGameCameraActor : public ACameraActor
{
	GENERATED_BODY()

public:

	AAmoebaPlayGameCameraActor();

	//Get a 2D vector representing the Height/Width of what is being current displayed on the screen
	FVector2D GetViewDimensions();

};
