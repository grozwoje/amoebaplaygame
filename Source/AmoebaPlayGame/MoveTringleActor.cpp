// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveTringleActor.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Engine/GameViewportClient.h"
#include "Components/PointLightComponent.h"
#include "DrawDebugHelpers.h"


AMoveTringleActor::AMoveTringleActor()
{
	PrimaryActorTick.bCanEverTick = true;

	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;

		FConstructorStatics()
			: BaseMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, OrangeMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;


	CustomMesh = CreateDefaultSubobject<UProceduralMeshComponent>("CustomMesh0");

	RootComponent = CustomMesh;


	CustomMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	CustomMesh->SetMaterial(0, ConstructorStatics.OrangeMaterial.Get());


	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();

	GenerateTriangleMesh();



}

void AMoveTringleActor::BeginPlay()
{
	Super::BeginPlay();
	//PointLight1->ToggleVisibility();

}

void AMoveTringleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMoveTringleActor::BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	if (ClickedComp)
	{

	}
}

void AMoveTringleActor::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{

}

void AMoveTringleActor::AddTriangle(int32 V1, int32 V2, int32 V3)
{
	Triangles.Add(V1);
	Triangles.Add(V2);
	Triangles.Add(V3);
}

void AMoveTringleActor::GenerateTriangleMesh()
{
	float a = 100.0f;
	Vertices.Add(FVector(.0f, .0f, .0f));
	Vertices.Add(FVector(.0f, a, .0f));
	Vertices.Add(FVector(a*FMath::Sqrt(3) / 2, 50.0f, .0f));

	AddTriangle(0, 1, 2);

	TArray<FVector2D> UV0;
	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(0, 0));

	TArray<FProcMeshTangent> tangents;
	tangents.Add(FProcMeshTangent(0, 0, 0));
	tangents.Add(FProcMeshTangent(0, 0, 0));
	tangents.Add(FProcMeshTangent(0, 0, 0));


	TArray<FVector> normals;
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));

	CustomMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, normals, UV0, VertexColors, tangents, true);

}


void AMoveTringleActor::ToggleLight()
{
	PointLight1->ToggleVisibility();

}