// Fill out your copyright notice in the Description page of Project Settings.


#include "AmoebaPawn.h"
#include "MoveTringleActor.h"
#include "AmoebaPlayGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"

AAmoebaPawn::AAmoebaPawn()
	: CP_Minus_2_First{ FVector(-12.0f, .0f, .0f) }
	, CP_Minus_2_Last{ FVector(-7.0f, 6.0f, .0f) }
	, BaseMinus{ FVector(-7.0f, .0f, .0f) }

	, CP_Main_First{ FVector(-3.0f, 5.0f, .0f) }
	, CP_Main_Core{ FVector(.0f, .0f, .0f) } //move point
	, Temp_Main_Core{ FVector(.0f, -1.0f ,0.f) }
	, CP_Main_Last{ FVector(3.0f, 4.0f, .0f) }

	, CP_Plus_2_First{ FVector(7.0f, 6.0f, .0f) }
	, CP_Plus_2_Last{ FVector(12.0f, .0f , .0f) }
	, BasePlus{ FVector(7.0f , .0f, .0f) }

	, N{ 16 }, BasePoint_0{ FVector(0.0f,6.f,.0f) }
{
	PrimaryActorTick.bCanEverTick = true;


	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> DarkGreenMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> PinkMaterial;

		FConstructorStatics()
			: BaseMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/DarkGreenMaterial.DarkGreenMaterial"))
			, DarkGreenMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/DarkGreenMaterial.DarkGreenMaterial"))
			, PinkMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/PinkMaterial.PinkMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));


	CP_Minus_2_Temp = 2 * (CP_Minus_2_Last - CP_Main_First) + CP_Main_Core;
	CP_Minus_1_Temp = 2 * CP_Main_First - CP_Main_Core;
	CP_Plus_1_Temp = 2 * CP_Main_Last - CP_Main_Core;
	CP_Plus_2_Temp = 2 * (CP_Plus_2_First - CP_Main_Last) + CP_Main_Core;

	MinusAomebaLeaf_2_Mesh = CreateDefaultSubobject<UProceduralMeshComponent>("MinusAomebaLeaf_2_Mesh");
	MinusAomebaLeaf_1_Mesh = CreateDefaultSubobject<UProceduralMeshComponent>("MinusAomebaLeaf_1_Mesh");
	MainAomebaLeafMesh = CreateDefaultSubobject<UProceduralMeshComponent>("MainAomebaLeafMesh");
	PlusAomebaLeaf_1_Mesh = CreateDefaultSubobject<UProceduralMeshComponent>("PlusAomebaLeaf_1_Mesh");
	PlusAomebaLeaf_2_Mesh = CreateDefaultSubobject<UProceduralMeshComponent>("PlusAomebaLeaf_2_Mesh");

	MinusTriangleMesh = CreateDefaultSubobject<UProceduralMeshComponent>("MinusTriangleMesh");
	PlusTriangleMesh = CreateDefaultSubobject<UProceduralMeshComponent>("PlusTriangleMesh");

	MinusAomebaLeaf_2_Mesh->SetupAttachment(RootComponent);
	MinusAomebaLeaf_1_Mesh->SetupAttachment(MinusAomebaLeaf_2_Mesh);
	MainAomebaLeafMesh->SetupAttachment(MinusAomebaLeaf_1_Mesh);

	MinusTriangleMesh->SetupAttachment(MinusAomebaLeaf_1_Mesh);

	PlusAomebaLeaf_1_Mesh->SetupAttachment(MainAomebaLeafMesh);
	PlusAomebaLeaf_2_Mesh->SetupAttachment(PlusAomebaLeaf_1_Mesh);

	PlusTriangleMesh->SetupAttachment(PlusAomebaLeaf_1_Mesh);
	
	MinusAomebaLeaf_2_Mesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	MinusAomebaLeaf_2_Mesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());
	
	MinusAomebaLeaf_1_Mesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	MinusAomebaLeaf_1_Mesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());

	MainAomebaLeafMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	MainAomebaLeafMesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());

	PlusAomebaLeaf_2_Mesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	PlusAomebaLeaf_2_Mesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());

	PlusAomebaLeaf_1_Mesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	PlusAomebaLeaf_1_Mesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());


	KernelMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("KernelMesh"));
	KernelMesh->SetStaticMesh(ShapeMeshAsset.Object);
	KernelMesh->SetupAttachment(PlusAomebaLeaf_1_Mesh);
	KernelMesh->SetRelativeLocation(FVector(.0f,150.0f, 100.0f));
	KernelMesh->SetRelativeScale3D(FVector(.2f, 1.2, 1.0f));
	KernelMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	KernelMesh->SetMaterial(0, ConstructorStatics.PinkMaterial.Get());

	MinusTriangleMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	MinusTriangleMesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());

	PlusTriangleMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	PlusTriangleMesh->SetMaterial(0, ConstructorStatics.DarkGreenMaterial.Get());

	MinusAomebaLeaf_2 = new BezierCurve3CPModel(CP_Minus_2_First, CP_Minus_2_Temp, CP_Minus_2_Last, N);
	MinusAomebaLeaf_1 = new BezierCurve3CPModel(CP_Minus_2_Last, CP_Minus_1_Temp, CP_Main_First, N);
	MainAomebaLeaf = new BezierCurve3CPModel(CP_Main_First, CP_Main_Core, CP_Main_Last, N);
	PlusAomebaLeaf_1 = new BezierCurve3CPModel(CP_Main_Last, CP_Plus_1_Temp, CP_Plus_2_First, N);
	PlusAomebaLeaf_2 = new BezierCurve3CPModel(CP_Plus_2_First, CP_Plus_2_Temp, CP_Plus_2_Last, N);

	CountdownTime = 1;
	CountdownTime_1 = 1;
	CountdownTime_2 = 1;


}

void AAmoebaPawn::BeginPlay()
{
	Super::BeginPlay();
	AAmoebaPlayGameGameModeBase* MyMode = Cast< AAmoebaPlayGameGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));

	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, FString::Printf(TEXT("Amoeba->CP_Minus_2_Temp ( %f, %f, %f) ")
		, this->CP_Minus_2_First.X, this->CP_Minus_2_First.Y, this->CP_Minus_2_First.Z));
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString::Printf(TEXT("Amoeba->CP_Minus_2_Temp :::::::( %f, %f, %f) ")
	, this->CP_Minus_2_Temp.X, this->CP_Minus_2_Temp.Y, this->CP_Minus_2_Temp.Z));
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::Printf(TEXT("Amoeba->CP_Minus_2_Temp ( %f, %f, %f) ")
	, this->CP_Minus_2_Last.X, this->CP_Minus_2_Last.Y, this->CP_Minus_2_Last.Z));
	//GEngine->GetWorld()->GetGameInstance()->GetName();
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("UGameplayStatics::GetGameInstance(this %s"), *MyMode->GetName()));
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("GetWorld()->GetName() %s"), *GetWorld()->GetName()));
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("GEngine->GetWorld()->GetGameInstance()->GetName() %s"), *UGameplayStatics::GetGameInstance(this)->GetName()));
	/* UGameplayStatics::OpenLevel((UObject*)GGameInstance, FName(TEXT("NewMap1")));
 
GGameInstance - is my global UMyGameInstance*, which i was initialized inside UMyGameInstance::Init().*/
	GenerateMinusAomebaLeaf_2_Mesh();
	GenerateMinusAomebaLeaf_1_Mesh();
	GenerateMainAomebaLeafMesh();
	GeneratePlusAomebaLeaf_1_Mesh();
	GeneratePlusAomebaLeaf_2_Mesh();


	GenerateMinusTriangle();
	GeneratePlusTriangle();

	this->SetActorScale3D(FVector(0.05f));
	this->SetActorLocationAndRotation(FVector(.0f, .0f, -130.0f), FRotator(.0f, 90.0f, .0f));

	LeftTriangle = GetWorld()->SpawnActor<AMoveTringleActor>(FVector(-250.0f,80.0f, .0f), FRotator(60.f, .0f, 90.0f));
	RightTriangle = GetWorld()->SpawnActor<AMoveTringleActor>(FVector(198.0f, 80.0f, -20.0f), FRotator(120.0f, 0.0f, 90.0f));

	LeftTriangle->SetActorScale3D(FVector(0.6f));
	RightTriangle->SetActorScale3D(FVector(.6f));

	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AAmoebaPawn::AdvanceTimer, 1.0f, true);
	GetWorldTimerManager().SetTimer(CountdownTimerHandle_1, this, &AAmoebaPawn::AdvanceTimer_1, 1.0f, true);
	GetWorldTimerManager().SetTimer(CountdownTimerHandle_2, this, &AAmoebaPawn::AdvanceTimer_2, 1.0f, true);

	MinusAomebaLeaf_2_Mesh->OnComponentHit.AddDynamic(this, &AAmoebaPawn::OnHit);
	MinusAomebaLeaf_1_Mesh->OnComponentHit.AddDynamic(this, &AAmoebaPawn::OnHit);
	MainAomebaLeafMesh->OnComponentHit.AddDynamic(this, &AAmoebaPawn::OnHit);
	PlusAomebaLeaf_2_Mesh->OnComponentHit.AddDynamic(this, &AAmoebaPawn::OnHit);
	PlusAomebaLeaf_1_Mesh->OnComponentHit.AddDynamic(this, &AAmoebaPawn::OnHit);

}

void AAmoebaPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Controller->CastToPlayerController()->GetHitResultUnderFinger(ETouchIndex::Touch1, ECC_Visibility, false, Hit);
	Controller->CastToPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	AomebaMove();

	{
		if (!CurrentVelocity.IsZero())
		{
			FVector NewLocation = GetActorLocation() + (CurrentVelocity * DeltaTime);
			SetActorLocation(NewLocation);
		}
	}
	if (Hit.IsValidBlockingHit() || Hit.Actor != nullptr)
	{
		if (Hit.Actor->IsA<AMoveTringleActor>())
		{
			if (Hit.Actor == LeftTriangle && GetActorLocation().X > -100.f)
			{
				SetActorLocation(FVector(GetActorLocation().X - 0.50f, GetActorLocation().Y, GetActorLocation().Z));
			}
			if (Hit.Actor == RightTriangle && GetActorLocation().X < 100.f)
			{
				SetActorLocation(FVector(GetActorLocation().X + 0.50f, GetActorLocation().Y, GetActorLocation().Z));
			}
		}
	}


	if (CountdownTime == 0)
	{

		CountdownTime = static_cast<float>(FMath::RandRange(1, 2));
		GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AAmoebaPawn::AdvanceTimer, 1.0f, true);


		TempBlock = GetWorld()->SpawnActor<ABlockTempActor>(ABlockTempActor::StaticClass());
		TempBlocks.Add(TempBlock);
		TempBlock->SetActorScale3D(FVector(0.3));
	}

	if (CountdownTime_1 == 0)
	{

		CountdownTime_1 = static_cast<float>(FMath::RandRange(1, 2));
		GetWorldTimerManager().SetTimer(CountdownTimerHandle_1, this, &AAmoebaPawn::AdvanceTimer_1, 1.0f, true);

		TempBlock_1 = GetWorld()->SpawnActor<ABlockTempActor>(FVector(80.0f, .0f, .0f), FRotator(0, 0, 0));
		TempBlocks_1.Add(TempBlock_1);
		TempBlock_1->SetActorScale3D(FVector(0.3));
	}

	if (CountdownTime_2 == 0)
	{


		CountdownTime_2 = static_cast<float>(FMath::RandRange(1, 2));
		GetWorldTimerManager().SetTimer(CountdownTimerHandle_2, this, &AAmoebaPawn::AdvanceTimer_2, 1.0f, true);

		TempBlock_2 = GetWorld()->SpawnActor<ABlockTempActor>(FVector(-80.0f, .0f, .0f), FRotator(0, 0, 0));
		TempBlocks_2.Add(TempBlock_2);
		TempBlock_2->SetActorScale3D(FVector(0.3));

	}
}

void AAmoebaPawn::SetupPlayerInputComponent(UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	const FInputAxisKeyMapping R_Key("MoveX", EKeys::D, 1.0f);
	const FInputAxisKeyMapping L_Key("MoveX", EKeys::A, -1.0f);

	UPlayerInput::AddEngineDefinedAxisMapping(R_Key);
	UPlayerInput::AddEngineDefinedAxisMapping(L_Key);

	InputComponent->BindAxis("MoveX", this, &AAmoebaPawn::MovePaddleX);

}

void AAmoebaPawn::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("$$$$$$$$$$$$$$$$$$$$")));


	if (OtherActor->IsA<ABlockTempActor>())
	{

		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, FString::Printf(TEXT("!!!!!!!!!!!!!!!!!!!!!!!!")));
		OtherActor->Destroy();
	}

}

void AAmoebaPawn::MovePaddleX(float Scale)
{

		CurrentVelocity.X = FMath::Clamp(Scale, -1.0f, 1.0f) * 100.0f;
	
}

void AAmoebaPawn::AddTriVertMinusAomebaLeaf_2(int32 V1, int32 V2, int32 V3)
{
	TriMinusAomebaLeaf_2.Add(V1);
	TriMinusAomebaLeaf_2.Add(V2);
	TriMinusAomebaLeaf_2.Add(V3);
}

void AAmoebaPawn::AddTriVertMinusAomebaLeaf_1(int32 V1, int32 V2, int32 V3)
{
	TriMinusAomebaLeaf_1.Add(V1);
	TriMinusAomebaLeaf_1.Add(V2);
	TriMinusAomebaLeaf_1.Add(V3);
}

void AAmoebaPawn::AddTriVertMainAomebaLeaf(int32 V1, int32 V2, int32 V3)
{
	TriMainAomebaLeaf.Add(V1);
	TriMainAomebaLeaf.Add(V2);
	TriMainAomebaLeaf.Add(V3);
}

void AAmoebaPawn::AddTriVertPlusAomebaLeaf_1(int32 V1, int32 V2, int32 V3)
{
	TriPlusAomebaLeaf_1.Add(V1);
	TriPlusAomebaLeaf_1.Add(V2);
	TriPlusAomebaLeaf_1.Add(V3);
}

void AAmoebaPawn::AddTriVertPlusAomebaLeaf_2(int32 V1, int32 V2, int32 V3)
{
	TriPlusAomebaLeaf_2.Add(V1);
	TriPlusAomebaLeaf_2.Add(V2);
	TriPlusAomebaLeaf_2.Add(V3);
}

void AAmoebaPawn::AddMinusTriangle(int32 V1, int32 V2, int32 V3)
{
	TriMinusTriangle.Add(V1);
	TriMinusTriangle.Add(V2);
	TriMinusTriangle.Add(V3);
}

void AAmoebaPawn::AddMPlusTriangle(int32 V1, int32 V2, int32 V3)
{
	TriPlusTriangle.Add(V1);
	TriPlusTriangle.Add(V2);
	TriPlusTriangle.Add(V3);
}

void AAmoebaPawn::GenerateMinusAomebaLeaf_2_Mesh()
{
	VertMinusAomebaLeaf_2.Add(FVector(.0f, BaseMinus.X * 100, BaseMinus.Y * 100));
	for (int i = 0; i <= MinusAomebaLeaf_2->Get_N(); i++) {
		VertMinusAomebaLeaf_2.Add(FVector(0, MinusAomebaLeaf_2->GetCoordinate(i).X * 100, MinusAomebaLeaf_2->GetCoordinate(i).Y * 100));
	}
	for (int t = 0; t < VertMinusAomebaLeaf_2.Num(); t++) {
		AddTriVertMinusAomebaLeaf_2(0, t + 1, t + 2);
	}
	MinusAomebaLeaf_2_Mesh->CreateMeshSection_LinearColor(0, VertMinusAomebaLeaf_2, TriMinusAomebaLeaf_2, TArray<FVector>(), TArray<FVector2D>(), VertColorsMinusAomebaLeaf_2, TArray<FProcMeshTangent>(), true);
}

void AAmoebaPawn::GenerateMinusAomebaLeaf_1_Mesh()
{
	VertMinusAomebaLeaf_1.Add(FVector(.0f, BaseMinus.X * 100, BaseMinus.Y * 100));
	for (int i = 0; i <= MinusAomebaLeaf_1->Get_N(); i++) {
		VertMinusAomebaLeaf_1.Add(FVector(0, MinusAomebaLeaf_1->GetCoordinate(i).X * 100, MinusAomebaLeaf_1->GetCoordinate(i).Y * 100));
	}
	for (int t = 0; t < VertMinusAomebaLeaf_1.Num(); t++) {
		AddTriVertMinusAomebaLeaf_1(0, t + 1, t + 2);
	}
	MinusAomebaLeaf_1_Mesh->CreateMeshSection_LinearColor(0, VertMinusAomebaLeaf_1, TriMinusAomebaLeaf_1, TArray<FVector>(), TArray<FVector2D>(), VertColorsMinusAomebaLeaf_1, TArray<FProcMeshTangent>(), true);
}

void AAmoebaPawn::GenerateMainAomebaLeafMesh()
{
	VertMainAomebaLeaf.Add(FVector(.0f, CP_Main_Core.X * 100, CP_Main_Core.Y * 100));
	for (int i = 0; i <= MinusAomebaLeaf_1->Get_N(); i++) {
		VertMainAomebaLeaf.Add(FVector(0, MainAomebaLeaf->GetCoordinate(i).X * 100, MainAomebaLeaf->GetCoordinate(i).Y * 100));

	}
	for (int t = 0; t < VertMainAomebaLeaf.Num(); t++) {
		AddTriVertMainAomebaLeaf(0, t + 1, t + 2);
	}
	MainAomebaLeafMesh->CreateMeshSection_LinearColor(0, VertMainAomebaLeaf, TriMainAomebaLeaf, TArray<FVector>(), TArray<FVector2D>(), VertColorsMainAomebaLeaf, TArray<FProcMeshTangent>(), true);
}

void AAmoebaPawn::GeneratePlusAomebaLeaf_1_Mesh()
{
	VertPlusAomebaLeaf_1.Add(FVector(.0f, BasePlus.X * 100, BasePlus.Y * 100));
	for (int i = 0; i <= PlusAomebaLeaf_1->Get_N(); i++) {
		VertPlusAomebaLeaf_1.Add(FVector(0, PlusAomebaLeaf_1->GetCoordinate(i).X * 100, PlusAomebaLeaf_1->GetCoordinate(i).Y * 100));

	}
	for (int t = 0; t < VertPlusAomebaLeaf_1.Num(); t++) {
		AddTriVertPlusAomebaLeaf_1(0, t + 1, t + 2);
	}
	PlusAomebaLeaf_1_Mesh->CreateMeshSection_LinearColor(0, VertPlusAomebaLeaf_1, TriPlusAomebaLeaf_1, TArray<FVector>(), TArray<FVector2D>(), VertColorsPlusAomebaLeaf_1, TArray<FProcMeshTangent>(), true);
}

void AAmoebaPawn::GeneratePlusAomebaLeaf_2_Mesh()
{
	VertPlusAomebaLeaf_2.Add(FVector(.0f, BasePlus.X * 100, BasePlus.Y * 100));
	for (int i = 0; i <= PlusAomebaLeaf_2->Get_N(); i++) {
		VertPlusAomebaLeaf_2.Add(FVector(0, PlusAomebaLeaf_2->GetCoordinate(i).X * 100, PlusAomebaLeaf_2->GetCoordinate(i).Y * 100));

	}
	for (int t = 0; t < VertPlusAomebaLeaf_2.Num(); t++) {
		AddTriVertPlusAomebaLeaf_2(0, t + 1, t + 2);
	}
	PlusAomebaLeaf_2_Mesh->CreateMeshSection_LinearColor(0, VertPlusAomebaLeaf_2, TriPlusAomebaLeaf_2, TArray<FVector>(), TArray<FVector2D>(), VertColorsPlusAomebaLeaf_2, TArray<FProcMeshTangent>(), true);
}

void AAmoebaPawn::GenerateMinusTriangle()
{
	//VertMinusTriangle.Add(FVector(BaseMinus, CP_Main_First, CP_Main_Core));
	VertMinusTriangle.Add(FVector(0.f, 100 * BaseMinus.X, 100 * BaseMinus.Y));
	VertMinusTriangle.Add(FVector(0.f, 100 * CP_Main_First.X, 100 * CP_Main_First.Y));
	VertMinusTriangle.Add(FVector(0.f, CP_Main_Core.X, CP_Main_Core.Y));

	AddMinusTriangle(0, 1, 2);

	MinusTriangleMesh->CreateMeshSection_LinearColor(0, VertMinusTriangle, TriMinusTriangle, TArray<FVector>(), TArray<FVector2D>(), VertColorsMinusTriangle, TArray<FProcMeshTangent>(), false);
}

void AAmoebaPawn::GeneratePlusTriangle()
{
	VertPlusTriangle.Add(FVector(0.f, 100 * BasePlus.X, 100 * BasePlus.Y));
	VertPlusTriangle.Add(FVector(0.f, 100 * CP_Main_Last.X, 100 * CP_Main_Last.Y));
	VertPlusTriangle.Add(FVector(0.f, CP_Main_Core.X, CP_Main_Core.Y));

	AddMPlusTriangle(0, 2, 1);

	PlusTriangleMesh->CreateMeshSection_LinearColor(0, VertPlusTriangle, TriPlusTriangle, TArray<FVector>(), TArray<FVector2D>(), VertColorsPlusTriangle, TArray<FProcMeshTangent>(), false);
}

void AAmoebaPawn::AomebaMove()
{

	float RunTime = GetGameTimeSinceCreation();

	CP_Main_Core += FVector(.0f, 0.001, 0);
	MainAomebaLeaf->Set_CP(1, CP_Main_Core);

	CP_Minus_1_Temp = 2 * CP_Main_First - CP_Main_Core;
	MinusAomebaLeaf_1->Set_CP(1, CP_Minus_1_Temp);

	CP_Plus_1_Temp = 2 * CP_Main_Last - CP_Main_Core;
	PlusAomebaLeaf_1->Set_CP(1, CP_Plus_1_Temp);

	CP_Minus_2_Temp = 2 * (CP_Minus_2_Last - CP_Main_First) + CP_Main_Core;
	MinusAomebaLeaf_2->Set_CP(1, CP_Minus_2_Temp);

	CP_Plus_2_Temp = 2 * (CP_Plus_2_First - CP_Main_Last) + CP_Main_Core;
	PlusAomebaLeaf_2->Set_CP(1, CP_Plus_2_Temp);

	float MinusTriSise = (BaseMinus.Y - CP_Main_First.Y) / (BaseMinus.X - CP_Main_First.X);
	float PlusTriSide = (BasePlus.Y - CP_Main_Last.Y) / (BasePlus.X - CP_Main_Last.X);

	float L_Minus = (CP_Main_Core.Y - CP_Minus_1_Temp.Y) / (CP_Main_Core.X - CP_Minus_1_Temp.X);
	float L_Plus = (CP_Main_Core.Y - CP_Plus_1_Temp.Y) / (CP_Main_Core.X - CP_Plus_1_Temp.X);;

	for (int TempVert = 0; TempVert < MainAomebaLeaf->Get_N(); TempVert++)
	{
		VertMinusAomebaLeaf_2[TempVert + 1] = FVector(0, MinusAomebaLeaf_2->GetCoordinate(TempVert).X * 100, MinusAomebaLeaf_2->GetCoordinate(TempVert).Y * 100);
		VertMinusAomebaLeaf_1[TempVert + 1] = FVector(0, MinusAomebaLeaf_1->GetCoordinate(TempVert).X * 100, MinusAomebaLeaf_1->GetCoordinate(TempVert).Y * 100);
		VertMainAomebaLeaf[TempVert + 1] = FVector(0, MainAomebaLeaf->GetCoordinate(TempVert).X * 100, MainAomebaLeaf->GetCoordinate(TempVert).Y * 100);
		VertPlusAomebaLeaf_1[TempVert + 1] = FVector(0, PlusAomebaLeaf_1->GetCoordinate(TempVert).X * 100, PlusAomebaLeaf_1->GetCoordinate(TempVert).Y * 100);
		VertPlusAomebaLeaf_2[TempVert + 1] = FVector(0, PlusAomebaLeaf_2->GetCoordinate(TempVert).X * 100, PlusAomebaLeaf_2->GetCoordinate(TempVert).Y * 100);
	}
	MinusAomebaLeaf_2_Mesh->UpdateMeshSection(0, VertMinusAomebaLeaf_2, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	MinusAomebaLeaf_1_Mesh->UpdateMeshSection(0, VertMinusAomebaLeaf_1, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	MainAomebaLeafMesh->UpdateMeshSection(0, VertMainAomebaLeaf, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	PlusAomebaLeaf_1_Mesh->UpdateMeshSection(0, VertPlusAomebaLeaf_1, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	PlusAomebaLeaf_2_Mesh->UpdateMeshSection(0, VertPlusAomebaLeaf_2, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());


	/////////
	if (MinusTriSise > L_Minus && Temp_Main_Core.Y <= CP_Main_Core.Y)
	{
		Temp_Main_Core = CP_Main_Core;
		CP_Main_Core += FVector(.0f, 0.01, 0);
	}
	else
	{
		Temp_Main_Core = CP_Main_Core;
		CP_Main_Core -= FVector(.0f, 0.01, 0);
	}
	//////
	if (PlusTriSide < L_Plus  && Temp_Main_Core.Y <= CP_Main_Core.Y)
	{
		Temp_Main_Core = CP_Main_Core;
		CP_Main_Core += FVector(.0f, 0.01, 0);
	}
	else
	{
		Temp_Main_Core = CP_Main_Core;
		CP_Main_Core -= FVector(.0f, 0.01, 0);
	}

	if (CP_Main_Core.Y < 0) 		CP_Main_Core += FVector(.0f, 0.01, 0);
}

void AAmoebaPawn::AdvanceTimer()
{
	--CountdownTime;
	if (CountdownTime < 1)
	{
		GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=====================================")));

	}
}




void AAmoebaPawn::AdvanceTimer_1()
{
	--CountdownTime_1;
	if (CountdownTime_1 < 1)
	{
		GetWorldTimerManager().ClearTimer(CountdownTimerHandle_1);
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=====================================")));

	}
}


void AAmoebaPawn::AdvanceTimer_2()
{
	--CountdownTime_2;
	if (CountdownTime_2 < 1)
	{
		GetWorldTimerManager().ClearTimer(CountdownTimerHandle_2);
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=====================================")));

	}
}
