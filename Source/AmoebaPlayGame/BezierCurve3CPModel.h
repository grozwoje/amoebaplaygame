// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
*
*/
class AMOEBAPLAYGAME_API BezierCurve3CPModel
{

public:
	BezierCurve3CPModel();
	BezierCurve3CPModel(FVector, FVector, FVector, int32, FVector);
	BezierCurve3CPModel(FVector, FVector, FVector, int32);
	~BezierCurve3CPModel();


	int32				Get_N(); // xxx
	void				Set_N(int32); // xxx

	FVector				Get_CP(int8 cp);
	void				Set_CP(int8 cp, FVector  coord);
	void				Set_CP(FVector coord_cp_0, FVector coord_cp_1, FVector coord_cp_2);

	FVector				GetCasteljauHornerPoint(float t);


	FVector				GetCoordinate(int32);
	FVector* 			GetCoordinatePointer(int32);

	FVector				GetBasePoint();
	//void				SetBasePoint(FVector bp);

private:
	int32				N;
	TArray<FVector>		N_Points;

	FVector		cp_0, cp_1, cp_2;
	FVector		BasePoint;

	void		InitBezier(FVector bp);
};
