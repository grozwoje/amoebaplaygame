// Fill out your copyright notice in the Description page of Project Settings.


#include "ElectricProtozoaActor.h"
#include "UObject/ConstructorHelpers.h"
#include "ProceduralMeshComponent.h"


AElectricProtozoaActor::AElectricProtozoaActor()
{
 	PrimaryActorTick.bCanEverTick = true;

	DownMesh = CreateDefaultSubobject<UProceduralMeshComponent>("DownMesh");
	DownMesh->SetupAttachment(RootComponent);

	UpMesh = CreateDefaultSubobject<UProceduralMeshComponent>("UpMesh");
	UpMesh->SetupAttachment(DownMesh);

	LeftMesh = CreateDefaultSubobject<UProceduralMeshComponent>("LeftMesh");
	LeftMesh->SetupAttachment(UpMesh);

	RightMesh = CreateDefaultSubobject<UProceduralMeshComponent>("RightMesh");
	RightMesh->SetupAttachment(LeftMesh);
	/*
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> PinkMaterial;

		FConstructorStatics()
			: BaseMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/DarkGreenMaterial.DarkGreenMaterial"))
			, PinkMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/PinkMaterial.PinkMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));

	CubeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CubeMesh"));
	CubeMesh->SetStaticMesh(ShapeMeshAsset.Object);
	CubeMesh->SetupAttachment(LeftMesh);
	CubeMesh->SetRelativeScale3D(FVector(.30));
	CubeMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	CubeMesh->SetMaterial(0, ConstructorStatics.PinkMaterial.Get());
	*/
	
}

void AElectricProtozoaActor::BeginPlay()
{
	Super::BeginPlay();
	GenerateDownMesh();
	//GenerateUpMesh();
	//GenerateLeftMesh();
	//GenerateRightMesh();
	//SetActorRelativeScale3D(FVector(0.3));
	//MyGameMode* MyMode = Cast< MyGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
}

void AElectricProtozoaActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	for (int32 index = 0; index < DownVertices.Num(); index++)

	for (int32 index = 0; index < UpVertices.Num(); index++)
	{
		DownVertices[index] += FVector(0, 0, -0.3);
		RightVertices[index] += FVector(0, 0, -0.3);
		UpVertices[index] += FVector(0, 0, -0.3);
		LeftVertices[index] += FVector(0, 0, -0.3);
	}
	RightMesh->UpdateMeshSection(0, RightVertices, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	DownMesh->UpdateMeshSection(0, DownVertices, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	UpMesh->UpdateMeshSection(0, UpVertices, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());
	LeftMesh->UpdateMeshSection(0, LeftVertices, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>());

}

void AElectricProtozoaActor::AddDownTriangle(int32 V1, int32 V2, int32 V3)
{
	DownTriangles.Add(V1);
	DownTriangles.Add(V2);
	DownTriangles.Add(V3);
}


void AElectricProtozoaActor::GenerateDownMesh()
{	
	DownVertices.Add(FVector(0, 0, 0)); 
	DownVertices.Add(FVector(50, 0, 0));
	DownVertices.Add(FVector(25, 100, 25*FMath::Sqrt(2)));

	AddDownTriangle(0, 1, 2);

	TArray<FLinearColor> VertexColors;
	VertexColors.Add(FLinearColor(0.f, 0.f, 1.f));

	DownMesh->CreateMeshSection_LinearColor(0, DownVertices, DownTriangles, TArray<FVector>(), TArray<FVector2D>(), VertexColors, TArray<FProcMeshTangent>(), true);
}
//====================


void AElectricProtozoaActor::AddUpTriangle(int32 V1, int32 V2, int32 V3)
{
	UpTriangles.Add(V1);
	UpTriangles.Add(V2);
	UpTriangles.Add(V3);
}


void AElectricProtozoaActor::GenerateUpMesh()
{
	UpVertices.Add(FVector(50, 0, 70));

	UpVertices.Add(FVector(0, 0,70));

	UpVertices.Add(FVector(25, 100, 25 * FMath::Sqrt(2)));

	AddUpTriangle(0, 1, 2);

	TArray<FLinearColor> VertexColors;
	VertexColors.Add(FLinearColor(0.f, 0.f, 1.f));

	UpMesh->CreateMeshSection_LinearColor(0, UpVertices, UpTriangles, TArray<FVector>(), TArray<FVector2D>(), VertexColors, TArray<FProcMeshTangent>(), true);
}
//=================================

void AElectricProtozoaActor::AddLeftTriangle(int32 V1, int32 V2, int32 V3)
{
	LeftTriangles.Add(V1);
	LeftTriangles.Add(V2);
	LeftTriangles.Add(V3);
}


void AElectricProtozoaActor::GenerateLeftMesh()
{
	LeftVertices.Add(FVector(50, 0, 70));

	LeftVertices.Add(FVector(25, 100, 25 * FMath::Sqrt(2)));

	LeftVertices.Add(FVector(50, 0, 0));

	AddLeftTriangle(0, 1, 2);

	TArray<FLinearColor> VertexColors;
	VertexColors.Add(FLinearColor(0.f, 0.f, 1.f));

	LeftMesh->CreateMeshSection_LinearColor(0, LeftVertices, LeftTriangles, TArray<FVector>(), TArray<FVector2D>(), VertexColors, TArray<FProcMeshTangent>(), true);
}



void AElectricProtozoaActor::AddRightTriangle(int32 V1, int32 V2, int32 V3)
{
	RightTriangles.Add(V1);
	RightTriangles.Add(V2);
	RightTriangles.Add(V3);
}


void AElectricProtozoaActor::GenerateRightMesh()
{
	RightVertices.Add(FVector(0, 0, 70));

	RightVertices.Add(FVector(0, 0, 0));

	RightVertices.Add(FVector(25, 100, 25 * FMath::Sqrt(2)));

	AddRightTriangle(0, 1, 2);

	TArray<FLinearColor> VertexColors;
	VertexColors.Add(FLinearColor(0.f, 0.f, 1.f));

	RightMesh->CreateMeshSection_LinearColor(0, RightVertices, RightTriangles, TArray<FVector>(), TArray<FVector2D>(), VertexColors, TArray<FProcMeshTangent>(), true);
}
