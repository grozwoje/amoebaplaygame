// Fill out your copyright notice in the Description page of Project Settings.


#include "DiatomActor.h"
#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/Engine.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/Actor.h"
#include "AmoebaPawn.h"
#include "Arena\BlockActor.h"
#include "Arena\BlockTempActor.h"
#include "Arena\BrickActor.h"
#include "Arena\CeilingActor.h"
#include "Materials/MaterialInstance.h"
#include "DrawDebugHelpers.h"

ADiatomActor::ADiatomActor()
{
	PrimaryActorTick.bCanEverTick = true;

	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> LightGreenMaterial;

		FConstructorStatics()
			: BaseMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, LightGreenMaterial(TEXT("/Game/StarterContent/Puzzle/Meshes/LightGreenMaterial.LightGreenMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;


	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShapeMeshAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	
	DiatomShape = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeMesh"));
	DiatomShape->SetStaticMesh(ShapeMeshAsset.Object);
	DiatomShape->SetupAttachment(RootComponent);
	//DiatomShape->BodyInstance.SetCollisionProfileName("Projectile");

	DiatomMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MoveShape"));
	DiatomMovement->UpdatedComponent = DiatomShape;

	DiatomMovement->InitialSpeed =180.f;
	DiatomMovement->MaxSpeed = 3000.f;
	DiatomMovement->bRotationFollowsVelocity = false;
	DiatomMovement->bShouldBounce = false;
	DiatomMovement->ProjectileGravityScale = 0.f;
	DiatomMovement->bConstrainToPlane = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));

	SphereComponent->SetupAttachment(DiatomShape);
	SphereComponent->InitSphereRadius(50.0f);
	SphereComponent->SetRelativeLocation(FVector(.0f, .0f, 50.0f));

	LightGreenMaterial = ConstructorStatics.LightGreenMaterial.Get();
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();

	DiatomShape->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());//
	DiatomShape->SetMaterial(0, ConstructorStatics.LightGreenMaterial.Get());


	CountdownTime = 2;
}
void ADiatomActor::BeginPlay()
{
	Super::BeginPlay();
	
	/*GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Amoeba->CP_Minus_2_Temp ( %f, %f) ")
		, Amoeba->CP_Minus_2_Temp.X, Amoeba->CP_Minus_2_Temp.Z));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Amoeba->CP_Minus_2_Temp ( %f, %f) ")
		, Amoeba->CP_Minus_2_Last.X, Amoeba->CP_Minus_2_Last.Z));*/
	
	this->SetActorScale3D(FVector(0.1f));
	this->SetActorLocation(FVector(10.0f, .0f, -15.f));
	P_T = FVector2D(10.0f, -15.0f);

	DiatomMovement->OnProjectileBounce.AddDynamic(this, &ADiatomActor::OnBounce);
	DiatomMovement->OnProjectileStop.AddDynamic(this, &ADiatomActor::OnStop);


	DiatomShape->OnComponentHit.AddDynamic(this, &ADiatomActor::OnHit);
	DiatomMovement->Velocity = FVector(0.0f, 0.0f, -80.0f);


	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &ADiatomActor::AdvanceTimer, 1.0f, true);

	Amoeba = Cast<AAmoebaPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

void ADiatomActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (GetActorLocation().Z < Amoeba->GetActorLocation().Z + 5.f)
	{
		class APlayerController * MyPC = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

		MyPC->ClientSetCameraFade(true, FColor::Black, FVector2D(1.0, 0.0), 1.0);

		this->SetActorLocation(FVector(30.0f, .0f, 80.0f));
		DiatomMovement->Velocity = FVector(0.0f, 0.0f, -30.0f);
		/*
		if (CountdownTime == 0)
		{
			CountdownTime = 2;
			GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &ADiatomActor::AdvanceTimer, 1.0f, true);
			this->SetActorLocation(FVector(.0f, .0f, 120.0f));
			// = 2;
		}*/
		//CountdownTime = 2;
	}
	/*
	if (this->GetActorLocation().Z > 110.f)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Diaitomis wery hight!!! "));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("CountdowTime %f"),CountdownTime);
		if (CountdownTime == 0)
		{
		
			//CountdownTime = 2;
			GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &ADiatomActor::AdvanceTimer, 1.0f, true);
			this->SetActorLocation(FVector(.0f, .0f, 120.0f));
		}
	}*/
}

void ADiatomActor::OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity)
{

}

void ADiatomActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	/*
	if (OtherActor->IsA<ABlockTempActor>())
	{

		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, FString::Printf(TEXT("+++++++++++++++++")));

		OtherActor->Destroy();
	}
	*/
}

void ADiatomActor::OnStop(const FHitResult& Hit)
{	
	if (Hit.Actor->IsA<AAmoebaPawn>())
	{		
		//float t = 0.0f;

		FVector AmoebaSize = Amoeba->GetActorScale3D();
		FVector DeltaH = Hit.Location - Amoeba->GetActorLocation();

		float X_DeltaH = DeltaH.X;
		float Y_DeltaH = DeltaH.Z;

		FVector2D P_Hit = FVector2D(X_DeltaH, Y_DeltaH);

		FVector2D P_F;
		FVector2D P_S;
		int32 TempNumber = 1;

		if (Amoeba->MinusAomebaLeaf_2_Mesh == Hit.GetComponent())
		{
			float SquareRadiusDelta = (Amoeba->VertMinusAomebaLeaf_2[1].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertMinusAomebaLeaf_2[1].Y*AmoebaSize.Y - X_DeltaH) +
				(Amoeba->VertMinusAomebaLeaf_2[1].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertMinusAomebaLeaf_2[1].Z*AmoebaSize.Z - Y_DeltaH);

			float TempSquareRadiusDelta = SquareRadiusDelta;

			for (int i = 1; i < Amoeba->MainAomebaLeaf->Get_N(); i++)
			{
				if (X_DeltaH > Amoeba->VertMinusAomebaLeaf_2[i].Y*AmoebaSize.Y)
				{
					SquareRadiusDelta =
						(Amoeba->VertMinusAomebaLeaf_2[i].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertMinusAomebaLeaf_2[i].Y*AmoebaSize.Y - X_DeltaH) +
						(Amoeba->VertMinusAomebaLeaf_2[i].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertMinusAomebaLeaf_2[i].Z*AmoebaSize.Z - Y_DeltaH);
					//UE_LOG(LogTemp, Warning, TEXT("     series of SquareRadiusDelta %i, ::::::: %f"), i, SquareRadiusDelta);
					if (SquareRadiusDelta < TempSquareRadiusDelta)
					{
						TempSquareRadiusDelta = SquareRadiusDelta;
						TempNumber++;
					}
				}
			}
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("TempNumber: %i"), TempNumber));
			//==============================>>>>>>>>>>>>
			//p''(t) = -2 * (p_2 - p_1) * (1 - t) + 2 * (p_1 - p_0)		
			float t = static_cast<float>(TempNumber) / static_cast<float>(Amoeba->MainAomebaLeaf->Get_N());//!!!!
			FVector ddp = 
				- 2 * (Amoeba->CP_Minus_2_Last - Amoeba->CP_Minus_2_Temp) * (1-t)
				+ 2 * (Amoeba->CP_Minus_2_Temp - Amoeba->CP_Minus_2_First);
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(">>>>>>>>>>>> t: %f"), t));
			//<<<<<<<<<<<<<<<===========================
			// //

			P_F = FVector2D(Amoeba->VertMinusAomebaLeaf_2[TempNumber].Y, Amoeba->VertMinusAomebaLeaf_2[TempNumber].Z);

			if (TempNumber == Amoeba->MainAomebaLeaf->Get_N())
				P_S = FVector2D(Amoeba->VertMinusAomebaLeaf_2[--TempNumber].Y, Amoeba->VertMinusAomebaLeaf_2[--TempNumber].Z);
			else	P_S = FVector2D(Amoeba->VertMinusAomebaLeaf_2[++TempNumber].Y, Amoeba->VertMinusAomebaLeaf_2[++TempNumber].Z);
		}

		if (Amoeba->MinusAomebaLeaf_1_Mesh == Hit.GetComponent())
		{
			float SquareRadiusDelta = (Amoeba->VertMinusAomebaLeaf_1[1].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertMinusAomebaLeaf_1[1].Y*AmoebaSize.Y - X_DeltaH) +
				(Amoeba->VertMinusAomebaLeaf_1[1].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertMinusAomebaLeaf_1[1].Z*AmoebaSize.Z - Y_DeltaH);

			float TempSquareRadiusDelta = SquareRadiusDelta;

			for (int i = 1; i < Amoeba->MainAomebaLeaf->Get_N(); i++)
			{
				if (X_DeltaH > Amoeba->VertMinusAomebaLeaf_1[i].Y*AmoebaSize.Y)
				{
					SquareRadiusDelta =
						(Amoeba->VertMinusAomebaLeaf_1[i].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertMinusAomebaLeaf_1[i].Y*AmoebaSize.Y - X_DeltaH) +
						(Amoeba->VertMinusAomebaLeaf_1[i].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertMinusAomebaLeaf_1[i].Z*AmoebaSize.Z - Y_DeltaH);
					//UE_LOG(LogTemp, Warning, TEXT("     series of SquareRadiusDelta %i, ::::::: %f"), i, SquareRadiusDelta);
					if (SquareRadiusDelta < TempSquareRadiusDelta)
					{
						TempSquareRadiusDelta = SquareRadiusDelta;
						TempNumber++;
					}
				}
			}
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("TempNumber: %i"), TempNumber));
			float t = static_cast<float>(TempNumber) / static_cast<float>(Amoeba->MainAomebaLeaf->Get_N());//!!!!
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(">>>>>>>>>>>> t: %f"), t));

			P_F = FVector2D(Amoeba->VertMinusAomebaLeaf_1[TempNumber].Y, Amoeba->VertMinusAomebaLeaf_1[TempNumber].Z);

			if (TempNumber == Amoeba->MainAomebaLeaf->Get_N())
				P_S = FVector2D(Amoeba->VertMinusAomebaLeaf_1[--TempNumber].Y, Amoeba->VertMinusAomebaLeaf_1[--TempNumber].Z);
			else	P_S = FVector2D(Amoeba->VertMinusAomebaLeaf_1[++TempNumber].Y, Amoeba->VertMinusAomebaLeaf_1[++TempNumber].Z);
		}

		if (Amoeba->MainAomebaLeafMesh == Hit.GetComponent())
		{
			float SquareRadiusDelta = (Amoeba->VertMainAomebaLeaf[1].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertMainAomebaLeaf[1].Y*AmoebaSize.Y - X_DeltaH) +
				(Amoeba->VertMainAomebaLeaf[1].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertMainAomebaLeaf[1].Z*AmoebaSize.Z - Y_DeltaH);

			float TempSquareRadiusDelta = SquareRadiusDelta;

			for (int i = 1; i < Amoeba->MainAomebaLeaf->Get_N(); i++)
			{
				if (X_DeltaH > Amoeba->VertMainAomebaLeaf[i].Y*AmoebaSize.Y)
				{
					SquareRadiusDelta =
						(Amoeba->VertMainAomebaLeaf[i].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertMainAomebaLeaf[i].Y*AmoebaSize.Y - X_DeltaH) +
						(Amoeba->VertMainAomebaLeaf[i].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertMainAomebaLeaf[i].Z*AmoebaSize.Z - Y_DeltaH);
					//UE_LOG(LogTemp, Warning, TEXT("     series of SquareRadiusDelta %i, ::::::: %f"), i, SquareRadiusDelta);
					if (SquareRadiusDelta < TempSquareRadiusDelta)
					{
						TempSquareRadiusDelta = SquareRadiusDelta;
						TempNumber++;
					}
				}
			}
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("TempNumber: %i"), TempNumber));
			float t = static_cast<float>(TempNumber) / static_cast<float>(Amoeba->MainAomebaLeaf->Get_N());//!!!!
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(">>>>>>>>>>>> t: %f"), t));
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("World delta for current frame equals %f"), GetWorld()->TimeSeconds));

			P_F = FVector2D(Amoeba->VertMainAomebaLeaf[TempNumber].Y, Amoeba->VertMainAomebaLeaf[TempNumber].Z);

			if (TempNumber == Amoeba->MainAomebaLeaf->Get_N())	
					P_S = FVector2D(Amoeba->VertMainAomebaLeaf[--TempNumber].Y, Amoeba->VertMainAomebaLeaf[--TempNumber].Z);
			else	P_S = FVector2D(Amoeba->VertMainAomebaLeaf[++TempNumber].Y, Amoeba->VertMainAomebaLeaf[++TempNumber].Z);
		}

		if (Amoeba->PlusAomebaLeaf_1_Mesh == Hit.GetComponent())
		{
			float SquareRadiusDelta = (Amoeba->VertPlusAomebaLeaf_1[1].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertPlusAomebaLeaf_1[1].Y*AmoebaSize.Y - X_DeltaH) +
				(Amoeba->VertPlusAomebaLeaf_1[1].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertPlusAomebaLeaf_1[1].Z*AmoebaSize.Z - Y_DeltaH);

			float TempSquareRadiusDelta = SquareRadiusDelta;

			for (int i = 1; i < Amoeba->MainAomebaLeaf->Get_N(); i++)
			{
				if (X_DeltaH > Amoeba->VertPlusAomebaLeaf_1[i].Y*AmoebaSize.Y)
				{
					SquareRadiusDelta =
						(Amoeba->VertPlusAomebaLeaf_1[i].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertPlusAomebaLeaf_1[i].Y*AmoebaSize.Y - X_DeltaH) +
						(Amoeba->VertPlusAomebaLeaf_1[i].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertPlusAomebaLeaf_1[i].Z*AmoebaSize.Z - Y_DeltaH);
					//UE_LOG(LogTemp, Warning, TEXT("     series of SquareRadiusDelta %i, ::::::: %f"), i, SquareRadiusDelta);
					if (SquareRadiusDelta < TempSquareRadiusDelta)
					{
						TempSquareRadiusDelta = SquareRadiusDelta;
						TempNumber++;
					}
				}
			}
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("TempNumber: %i"), TempNumber));
			float t = static_cast<float>(TempNumber) / static_cast<float>(Amoeba->MainAomebaLeaf->Get_N());//!!!!
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(">>>>>>>>>>>> t: %f"), t));

			P_F = FVector2D(Amoeba->VertPlusAomebaLeaf_1[TempNumber].Y, Amoeba->VertPlusAomebaLeaf_1[TempNumber].Z);

			if (TempNumber == Amoeba->MainAomebaLeaf->Get_N())
				P_S = FVector2D(Amoeba->VertPlusAomebaLeaf_1[--TempNumber].Y, Amoeba->VertPlusAomebaLeaf_1[--TempNumber].Z);
			else	P_S = FVector2D(Amoeba->VertPlusAomebaLeaf_1[++TempNumber].Y, Amoeba->VertPlusAomebaLeaf_1[++TempNumber].Z);
		}

		if (Amoeba->PlusAomebaLeaf_2_Mesh == Hit.GetComponent())
		{
			float SquareRadiusDelta = (Amoeba->VertPlusAomebaLeaf_2[1].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertPlusAomebaLeaf_2[1].Y*AmoebaSize.Y - X_DeltaH) +
				(Amoeba->VertPlusAomebaLeaf_2[1].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertPlusAomebaLeaf_2[1].Z*AmoebaSize.Z - Y_DeltaH);

			float TempSquareRadiusDelta = SquareRadiusDelta;

			for (int i = 1; i < Amoeba->MainAomebaLeaf->Get_N(); i++)
			{
				if (X_DeltaH > Amoeba->VertPlusAomebaLeaf_2[i].Y*AmoebaSize.Y)
				{
					SquareRadiusDelta =
						(Amoeba->VertPlusAomebaLeaf_2[i].Y*AmoebaSize.Y - X_DeltaH) * (Amoeba->VertPlusAomebaLeaf_2[i].Y*AmoebaSize.Y - X_DeltaH) +
						(Amoeba->VertPlusAomebaLeaf_2[i].Z*AmoebaSize.Z - Y_DeltaH) * (Amoeba->VertPlusAomebaLeaf_2[i].Z*AmoebaSize.Z - Y_DeltaH);
					//UE_LOG(LogTemp, Warning, TEXT("     series of SquareRadiusDelta %i, ::::::: %f"), i, SquareRadiusDelta);
					if (SquareRadiusDelta < TempSquareRadiusDelta)
					{
						TempSquareRadiusDelta = SquareRadiusDelta;
						TempNumber++;
					}
				}
			}
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("TempNumber: %i"), TempNumber));
			float t = static_cast<float>(TempNumber) / static_cast<float>(Amoeba->MainAomebaLeaf->Get_N());//!!!!
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(">>>>>>>>>>>> t: %f"), t));

			P_F = FVector2D(Amoeba->VertPlusAomebaLeaf_2[TempNumber].Y, Amoeba->VertPlusAomebaLeaf_2[TempNumber].Z);

			if (TempNumber == Amoeba->MainAomebaLeaf->Get_N())
				P_S = FVector2D(Amoeba->VertPlusAomebaLeaf_2[--TempNumber].Y, Amoeba->VertPlusAomebaLeaf_2[--TempNumber].Z);
			else	P_S = FVector2D(Amoeba->VertPlusAomebaLeaf_2[++TempNumber].Y, Amoeba->VertPlusAomebaLeaf_2[++TempNumber].Z);
		}

		TheoremOfCosines(P_Hit, P_F, P_T);
		float s = (FVector2D::Distance(P_T, P_Hit)) * TheoremOfCosines(P_Hit, P_F, P_T) ;
		float beta = FMath::Atan( DirectionalCoordinate(P_F, P_S ));
		float P_TT_X = s * FMath::Cos(beta);
		float P_TT_Y = s * FMath::Sin(beta);
		float r = TheoremOfCosines(s, FVector2D::Distance(P_Hit, P_T), beta);
		FVector2D P_TT = FVector2D(P_TT_X, P_TT_Y);
		float P_TN_X = r * FMath::Sin(beta + (3.14159/2)) + P_TT.X;
		float P_TN_Y = r * FMath::Cos(beta + (3.14159/2)) + P_TT.Y;
		FVector2D P_TN = FVector2D(P_TN_X, P_TN_Y);

		SetActorLocation(FVector(GetActorLocation().X, 0, GetActorLocation().Z +5.f));			//DiatomMovement->Velocity = FVector(P_TN_X, 0.0f, P_TN_Y);
		Trace = FVector(P_TN_Y, 0.0f, P_TN_X);
		DiatomMovement->Velocity = Trace ;

		DiatomMovement->SetUpdatedComponent(DiatomShape);
		DiatomMovement->UpdateComponentVelocity();
	}

	if (Hit.Actor->IsA<ABlockActor>())
	{
		//DiatomMovement->bShouldBounce = true;
		
		ABlockActor* Block = Cast<ABlockActor>(Hit.Actor);
		Trace.X = -Trace.X;
		Trace.Z = -Trace.Z;
		DiatomMovement->SetUpdatedComponent(DiatomShape);
		DiatomMovement->Velocity = Trace;
		DiatomMovement->UpdateComponentVelocity();
		Block->RandomReplaceActorLocation();

	}

	if (Hit.Actor->IsA<ABlockTempActor>())
	{
		//DiatomMovement->bShouldBounce = true;

		ABlockTempActor* TempBlock = Cast<ABlockTempActor>(Hit.Actor);
		//Hit.Actor->Destroy();
		Trace.X = -Trace.X;
		Trace.Z = -Trace.Z;
		DiatomMovement->SetUpdatedComponent(DiatomShape);
		DiatomMovement->Velocity = Trace;
		DiatomMovement->UpdateComponentVelocity();
		//TempBlock->RandomReplaceActorLocation();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Hit Temp Block"));

	}
	
	if (Hit.Actor->IsA<ABrickActor>())
	{
		Trace.X = -Trace.X;
		DiatomMovement->SetUpdatedComponent(DiatomShape);
		DiatomMovement->Velocity = Trace;
		DiatomMovement->UpdateComponentVelocity();
	}
	
	if (Hit.Actor->IsA<ACeilingActor>())
	{
		Trace.Z = -Trace.Z;
		DiatomMovement->SetUpdatedComponent(DiatomShape);
		DiatomMovement->Velocity =Trace;
		DiatomMovement->UpdateComponentVelocity();
	}
	if (Hit.Actor->IsA<ADiatomActor>())
	{ 
		Trace.Z = -Trace.Z;
		Trace.X = -Trace.X;
		DiatomMovement->SetUpdatedComponent(DiatomShape);
		DiatomMovement->Velocity = Trace;
		DiatomMovement->UpdateComponentVelocity();
	}

	DiatomMovement->Velocity = Trace * 3;
	DiatomMovement->UpdateComponentVelocity();

}

FVector ADiatomActor::GeneralLineEquation(FVector2D& First, FVector2D& Second)
{
	//Ax + By + C = 0
	//FVector(A,B,C)
	//a = -A
	float A = .0f;
	float B = .0f;
	float C = .0f;
	if (First.X != Second.X)
	{
		A = -(First.Y - Second.Y) / (First.X - Second.X);
		B = 1.0;
		C = -A * First.X - First.Y;
	}
	else
	{
		A = 1;
		B = .0f;
		C = -First.X;
	}
	UE_LOG(LogTemp, Warning, TEXT("GeneralLineEquation A: %f B: %f, C: %f"), A, B, C);

	return FVector(A,B,C);
}

FVector2D ADiatomActor::IntersectionLines(FVector& Str_1, FVector& Str_2)
{
	//x = A, y = B, z = C
	float X = (Str_2.Z / Str_2.Y - Str_1.Z/ Str_1.Y) / (Str_1.X / Str_1.Y - Str_2.X / Str_2.Y);
	float Y = (Str_2.Z / Str_2.X - Str_1.Y / Str_1.X) / (Str_1.Y / Str_1.X - Str_2.Y / Str_2.X );
	return FVector2D(X,Y);
}

float ADiatomActor::PerpendicularLineFactor(float& A)
{
	if(A !=0) return -1/A;
	else return 1;//!!!
}

float ADiatomActor::ShiftFactor(FVector& Line, FVector2D& Point)
{
	//Ax + By + C = 0
	//B = (-Ax - C) / y
	float B = (-Line.X * Point.X - Line.Z) / Point.Y;
	//PerpendicularLineFactor(A);

	return B;
}

FVector2D ADiatomActor::PointOnPerpendicularLine()
{
	//....
	return FVector2D();
}


//------------------------------------------------


float ADiatomActor::DirectionalCoordinate(FVector2D& P_1, FVector2D& P_2)
{

	if (P_1.X == P_2.X) 
	{
		P_1.X = P_1.X + 0.0002;
	}
	return (P_2.Y - P_1.Y) / (P_2.X - P_1.X);;
}

float ADiatomActor::ShiftCoodinate(FVector2D& P_1, FVector2D& P_2)
{
	return  P_2.Y - DirectionalCoordinate(P_1, P_2)*P_2.X;	
}

float ADiatomActor::TheoremOfCosines(FVector2D P_A/*P_H*/, FVector2D P_B, FVector2D P_C)
{
	//cos<P_A
	float a = FVector2D::Distance(P_B, P_C);

	float b = FVector2D::Distance(P_A, P_B);
	float c = FVector2D::Distance(P_A, P_C);

	return (a*a-b*b-c*c)/(2*b*c);
}

float ADiatomActor::TheoremOfCosines(float b, float c, float angle)
{
	return  FMath::Sqrt(b*b + c*c - 2 * b*c * FMath::Cos(angle));
}

void ADiatomActor::AdvanceTimer()
{
	--CountdownTime;
	if (CountdownTime <2)
	{
		GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("=========%f"), CountdownTime));

	}
}

void ADiatomActor::VaightAction()
{

}