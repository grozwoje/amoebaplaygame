// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PointsActor.generated.h"

UCLASS()
class AMOEBAPLAYGAME_API APointsActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APointsActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//How long, in seconds, the countdown will run
	UPROPERTY(EditAnywhere)
	int32 CountdownTime;

	class UTextRenderComponent* PointText;


private:
	int32 Points;
public:
	int32	GetPoints();
	void	SetPoints(int32 poiSnts);
	void	AddPoint();

};
