// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "AmoebaPlayGameHUD.generated.h"

/**
*
*/
UCLASS()
class AMOEBAPLAYGAME_API AAmoebaPlayGameHUD : public AHUD
{
	GENERATED_BODY()
protected:

	TSharedPtr<class SAmoebaPlayGameMenuWidget> MenuWidget;
	TSharedPtr<class SWidget> MenuWidgetContainer;

	virtual void BeginPlay() override;

public:
	void ShowMenu();
	void RemoveMenu();
};
